import React from "react";
//import datastore from "../utils/datastore.json";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Redirect } from "react-router-dom";
import DashboardPage from "./components/dashboard/DashboardPage";
import EMShellBar from "./components/maindashboard/EMShellBar";
import Settings from "./components/maindashboard/Settings";
import { Loader } from "@ui5/webcomponents-react/lib/Loader";
import Events from "./components/maindashboard/Events";
import datastorage from "./utils/datastorage.json";
import equipmentNotifications from "./utils/equipmentNotifications.json";
import { proxy } from "./utils/constants";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.createDashboard = this.createDashboard.bind(this);
    this.removeDashboard = this.removeDashboard.bind(this);
    this.createGadget = this.createGadget.bind(this);
    this.removeGadget = this.removeGadget.bind(this);
    this.updateGadget = this.updateGadget.bind(this);
    this.state = {
      gadgets: [],
      dashboards: [],
      loading: false,
      postID: 1,
      events: [],
      newNotifications: [],
      notificationCounter: 0,
      settings: {
        offPeakStart: 8,
        offPeakEnd: 18,
        line1Target: 21,
        line2Target: 20,
        line3Target: 39,
        allLinesTarget: 80,
        exceedTarget: 200,
      },
    };
  }

  hanaURL =
    "https://95d72361trial-95d72361trial-dev-energymanager-srv.cfapps.eu10.hana.ondemand.com";
  proxyUrl = proxy;

  useLocalData = false;

  changeSettings = (newSettings) => {
    this.setState({ settings: newSettings }, () =>
      this.pushToStorage(
        {
          gadgets: this.state.gadgets,
          dashboards: this.state.dashboards,
          settings: this.state.settings,
        },
        this.state.postID
      )
    );
  };

  removeGadget = (id) => {
    //TODO call backendAPI.removeGadget , then:
    this.setState(
      (currentState) => ({
        gadgets: currentState.gadgets.filter((g) => g.id !== id),
      }),
      () => {
        // In the callback of setState so we are certain the state has actually been changed
        this.pushToStorage(
          {
            gadgets: this.state.gadgets,
            dashboards: this.state.dashboards,
            settings: this.state.settings,
          },
          this.state.postID
        );
      }
    );
  };

  createGadget = (gadget) => {
    console.log("Gadget created: ", gadget);
    this.setState(
      (currentState) => ({
        gadgets: currentState.gadgets.concat([gadget]),
      }),
      () => {
        // In the callback of setState so we are certain the state has actually been changed
        this.pushToStorage(
          {
            gadgets: this.state.gadgets,
            dashboards: this.state.dashboards,
            settings: this.state.settings,
          },
          this.state.postID
        );
      }
    );
  };

  updateGadget = (gadget) => {
    this.setState((currentState) => ({
      gadgets: currentState.gadgets.filter((g) => g.id !== gadget.id),
    }));
    this.setState(
      (currentState) => ({
        gadgets: currentState.gadgets.concat([gadget]),
      }),
      () => {
        // In the callback of setState so we are certain the state has actually been changed
        this.pushToStorage(
          {
            gadgets: this.state.gadgets,
            dashboards: this.state.dashboards,
            settings: this.state.settings,
          },
          this.state.postID
        );
      }
    );
  };

  createDashboard = (db) => {
    this.setState(
      (currentState) => ({
        dashboards: currentState.dashboards.concat([db]),
      }),
      () => {
        // In the callback of setState so we are certain the state has actually been changed
        this.pushToStorage(
          {
            gadgets: this.state.gadgets,
            dashboards: this.state.dashboards,
            settings: this.state.settings,
          },
          this.state.postID
        );
      }
    );
  };

  fetchNotification = (equipmentId) => {
    return new Promise((resolve, reject) => {
      if (this.useLocalData) {
        resolve({
          equipmentId: equipmentId,
          notificationData: equipmentNotifications[equipmentId - 1],
        });
      }
      try {
        fetch(
          this.proxyUrl +
            this.hanaURL +
            `/event/Equipments_Average_Notification(id=${equipmentId},percentage=${this.state.settings.exceedTarget})/Set`
        )
          .then((blob) => blob.json())
          .then((data) => {
            resolve({
              equipmentId: equipmentId,
              notificationData: data.value,
            });
          });
      } catch (error) {
        reject(error);
      }
    });
  };

  fetchSingleFromStorage = (id) => {
    return new Promise((resolve, reject) => {
      try {
        fetch(this.proxyUrl + this.hanaUrl + `/storage/Data_storage/${id}`)
          .then((blob) => blob.json())
          .then((data) => {
            resolve(data.value);
          });
      } catch (error) {
        reject(error);
      }
    });
  };

  deleteSingleFromStorage = (id) => {
    return new Promise((resolve, reject) => {
      try {
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("origin", this.proxyUrl);

        const init = {
          method: "DELETE",
          headers,
        };

        fetch(
          this.proxyUrl + this.hanaUrl + `/storage/Data_storage/${id}`,
          init
        ).then((response) => resolve(response));
      } catch (error) {
        console.log("error while deleting from backend", error);
        reject(error);
      }
    });
  };

  fetchStorage = () => {
    return new Promise((resolve, reject) => {
      if (this.useLocalData) {
        resolve(datastorage);
      }
      try {
        fetch(this.proxyUrl + this.hanaURL + "/storage/Data_storage")
          .then((blob) => blob.json())
          .then((data) => {
            console.log("Data fetched from the backend", data);
            resolve(data.value);
          });
      } catch (error) {
        console.log(error);
        resolve(datastorage);
      }
    });
  };

  getState = (datastore) => {
    return new Promise((resolve, reject) => {
      try {
        //const datastorage = data.filter((item) => item.ID < 90000).sort((a,b)=> b.ID-a.ID)[0]
        const datastoreParsed = JSON.parse(datastore[0].value);
        console.log("DATASTORE", datastoreParsed);
        const newSettings = datastoreParsed.settings
          ? datastoreParsed.settings
          : this.state.settings;
        this.setState(
          {
            gadgets: datastoreParsed.gadgets,
            dashboards: datastoreParsed.dashboards,
            gadgettypes: datastoreParsed.gadgettypes,
            measuretypes: datastoreParsed.measuretypes,
            scopetypes: datastoreParsed.scopetypes,
            loading: false,
            settings: newSettings,
          },
          () => resolve(this.state)
        );
      } catch (error) {
        reject(error);
      }
    });
  };

  pushToStorage = (object, id) => {
    const idString = "" + id;
    // Store a new entry in the backend
    return new Promise((resolve, reject) => {
      try {
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("origin", this.proxyUrl);

        const newEntry = JSON.stringify(object);
        const now = new Date();
        const nowString = now.toISOString();

        const body =
          `{
      "createdAt": "` +
          nowString +
          `",
      "createdBy": "anonymous",
      "modifiedAt": "` +
          nowString +
          `",
      "modifiedBy": "anonymous",
      "ID": ${id},
      "value": "` +
          newEntry.replace(/[\"]/g, '\\"').replace(/\\n/g, "") +
          `"
    }`;

        console.log("BODY to be pushed: ", body);

        const init = {
          method: "PUT",
          headers,
          body,
        };

        fetch(
          this.proxyUrl + this.hanaURL + "/storage/Data_storage/" + idString,
          init
        )
          .then((response) => response.json())
          .then((data) => {
            resolve(data.value);
          });
      } catch (error) {
        console.log("error while pushing to backend", error);
        reject(error);
      }
    });
  };

  componentDidMount() {
    // Fetch storage from the backend
    this.fetchStorage().then((res) => {
      let fetchedStorage = res;
      if (fetchedStorage.length < 1) {
        // dummy entry
        fetchedStorage = datastorage;
      }
      const maxID = fetchedStorage[fetchedStorage.length - 1].ID;
      let currentState = {};
      this.setState({ postID: maxID });
      currentState = fetchedStorage.filter(
        (storageEntry) => storageEntry.ID === maxID
      );
      // Parse the value and put all items into the state
      this.getState(currentState).then(() => {
        // Fetch notifications for all equipments from the backend
        let fetchPromises = [];
        for (let i = 1; i <= 3; i++) {
          fetchPromises.push(this.fetchNotification(i));
        }
        // Executes after all fetchPromises are fulfilled and the notifications are available
        Promise.all(fetchPromises).then((notifications) => {
          console.log(
            "Notifications fetched from the backend: ",
            notifications
          );

          // Set today and yesterday to determine if a new notification for yesterday must be raised
          // If the application is actually used remove the arguments from the 2 new Date constructors to get the actual date
          const today = new Date(2020, 1, 1);
          today.setHours(0, 0, 0, 0);
          const yesterday = new Date(2020, 1, 1);
          yesterday.setHours(0, 0, 0, 0);
          yesterday.setDate(yesterday.getDate() - 1);

          let fetchedEvents = [];
          // For each notification one event is created (=> one event per day that a notification came up)
          notifications.forEach((equipmentSpecificNotifications) => {
            equipmentSpecificNotifications.notificationData.forEach(
              (notification) => {
                const date = new Date(notification.year, 0); // initialize a date in `year-01-01`
                // Convert the day of the year (day field in the fetched notification) to an actual date
                const notificationDate = new Date(
                  date.setDate(notification.day)
                );
                if (notificationDate.getTime() !== yesterday.getTime()) {
                  fetchedEvents.push({
                    equipment_ID: notification.equipment_ID,
                    date: notificationDate,
                    cumulated_pos_consumption:
                      notification.cumulated_pos_consumption,
                    exceedTarget: this.state.settings.exceedTarget,
                  });
                }

                // If an event references yesterday put it into newNotifications to also show it in the ShellBar
                if (notificationDate.getTime() === yesterday.getTime()) {
                  console.log(
                    "notificationDate is yesterday for equipment: ",
                    notification.equipment_ID
                  );
                  this.setState({
                    newNotifications: this.state.newNotifications.concat({
                      equipment_ID: notification.equipment_ID,
                      date: notificationDate,
                      cumulated_pos_consumption:
                        notification.cumulated_pos_consumption,
                      exceedTarget: this.state.settings.exceedTarget,
                    }),
                    notificationCounter: this.state.notificationCounter + 1,
                  });
                }
              }
            );
          });
          // Sort fetchedEvents by date
          fetchedEvents.sort(function (a, b) {
            // Turn your strings into dates, and then subtract them
            // to get a value that is either negative, positive, or zero.
            return new Date(b.date) - new Date(a.date);
          });
          // Put all fetchedEvents into the state
          this.setState({ events: fetchedEvents });
        });
      });
    });
  }

  removeDashboard = (dashboard) => {
    //TODO call backendAPI.removeGadget , then:
    if (dashboard) {
      this.setState(
        (currentState) => ({
          dashboards: currentState.dashboards.filter(
            (db) => db.id !== dashboard.id
          ),
          gadgets: currentState.gadgets.filter(
            (g) => g.dashboard !== dashboard.id
          ),
        }),
        () => {
          // In the callback of setState so we are certain the state has actually been changed
          this.pushToStorage(this.state, this.state.postID);
          //this.pushStorage()
        }
      );
    }
  };

  UNSAFE_componentWillMount() {
    if (!this.useLocalData) {
      this.setState({ loading: true });
    }
  }

  render() {
    const { loading } = this.state;
    if (loading) {
      return (
        <Router>
          <EMShellBar />
          <Loader />
        </Router>
      );
    } else {
      return (
        <Router>
          <EMShellBar
            notificationCounter={this.state.notificationCounter}
            newNotifications={this.state.newNotifications}
            dashboards={this.state.dashboards}
          />
          <Switch>
            <Route
              exact
              path="/"
              render={() => <Redirect to="/systemDashboard" />}
            />
            <Route
              exact
              path="/settings"
              render={() => (
                <Settings
                  onChangeSettings={this.changeSettings}
                  settings={this.state.settings}
                />
              )}
            />
            <Route
              exact
              path="/events"
              render={() => (
                <Events
                  newNotifications={this.state.newNotifications}
                  events={this.state.events}
                  notificationCounter={this.state.notificationCounter}
                  settings={this.state.settings}
                />
              )}
            />
            <Route
              path="/:id"
              render={({ match }) => (
                <DashboardPage
                  dashboard={
                    this.state.dashboards[parseInt(match.params.id) - 1]
                  }
                  dashboards={this.state.dashboards}
                  onDeleteGadget={this.removeGadget}
                  onAddGadget={this.createGadget}
                  onUpdateGadget={this.updateGadget}
                  onCreateDashboard={(db) => {
                    this.createDashboard(db);
                  }}
                  onDeleteDashboard={this.removeDashboard}
                  gadgets={this.state.gadgets}
                  settings={this.state.settings}
                  proxy={this.proxyUrl}
                  hanaURL={this.hanaURL}
                />
              )}
            />
          </Switch>
        </Router>
      );
    }
  }
}

export default App;
