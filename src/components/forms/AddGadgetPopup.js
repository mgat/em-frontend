import React, { useRef, useState, useCallback } from "react";
import {
  Dialog,
  FormItem,
  Input,
  InputType,
  Select,
  Option,
  FormGroup,
  Form,
} from "@ui5/webcomponents-react/";
import "@ui5/webcomponents-icons/dist/icons/add";
import styled from "styled-components";

function AddGadgetPopup(props) {
  console.log("props received in AddGadgetPopup: " + JSON.stringify(props));
  const hanaRoute =
    props.hanaURL ||
    "https://95d72361trial-95d72361trial-dev-energymanager-srv.cfapps.eu10.hana.ondemand.com";
  const dialogRef = useRef();
  const openDialog = useCallback(() => {
    dialogRef.current.open();
  }, [dialogRef]);
  const closeDialog = useCallback(() => {
    dialogRef.current.close();
  }, [dialogRef]);

  const [titleInput, setTitleInput] = useState("");
  const [typeInput, setTypeInput] = useState("Text Field");
  const [measureInput, setMeasureInput] = useState(
    props.measureTypes[0] || "Energy Consumption"
  );
  const [scopeInput, setScopeInput] = useState(
    props.scopeTypes.consumption[0] || "AllTime_Hourly"
  );
  const [targetInput, setTargetInput] = useState(0);
  const [scopeTypes, setScopeTypes] = useState(props.scopeTypes.consumption);

  const now = new Date();
  let dd = String(now.getDate()).padStart(2, "0");
  let mm = String(now.getMonth() + 1).padStart(2, "0"); //January is 0!
  let yyyy = now.getFullYear();

  // Calculate the date of the previous monday (needed for calculations of "this week")
  let prevMonday = now;
  prevMonday.setDate(prevMonday.getDate() - ((prevMonday.getDay() + 6) % 7));
  let prevMondaydd = String(prevMonday.getDate()).padStart(2, "0");
  let prevMondaymm = String(prevMonday.getMonth() + 1).padStart(2, "0");
  let prevMondayyyy = prevMonday.getFullYear();

  const handleSubmit = () => {
    let kpiGadgetTemplate = {
      id: props.gadgetsCount + 1,
      title: titleInput,
      dashboard: props.dashboardId,
      type: typeInput,
      target: targetInput,
      attributes: {
        measure: measureInput,
        scope: scopeInput,
        url: `${hanaRoute}/browse/Sensor_Data`,
      },
    };
    // While we do not have live data
    yyyy = 2020;
    mm = "05";
    dd = 10;
    prevMondayyyy = 2020;
    prevMondaymm = "05";
    prevMondaydd = "04";
    if (props.onAddGadget) {
      closeDialog();
      // Check what kind of gadget is to be created and create the corresponding object
      if (typeInput === "KPI") {
        console.log("scopeInput", scopeInput);
        if (measureInput === "Energy Consumption") {
          // Set URL depending on the scope
          switch (scopeInput) {
            case "AllTime_Hourly":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Hourly`;
              break;
            case "AllTime_Daily":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Daily`;
              break;
            case "AllTime_Weekly":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Weekly`;
              break;
            case "AllTime_Monthly":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Monthly`;
              break;
            case "AllTime_Quarterly":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Quaterly`;
              break;
            case "AllTime_Yearly":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Yearly`;
              break;
            case "Hourly_ThisWeek":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Hourly_by_Date(start_Date=${prevMondayyyy}-${prevMondaymm}-${prevMondaydd}T00:00:00.000Z,end_Date=${yyyy}-${mm}-${dd}T23:59:59.000Z)/Set`;
              break;
            case "Hourly_ThisMonth":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Hourly_by_Date(start_Date=${yyyy}-${mm}-01T00:00:00.000Z,end_Date=${yyyy}-${mm}-${dd}T23:59:59.000Z)/Set`;
              break;
            case "Hourly_ThisYear":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Hourly_by_Date(start_Date=${yyyy}-01-01T00:00:00.000Z,end_Date=${yyyy}-${mm}-${dd}T23:59:59.000Z)/Set`;
              break;
            case "Daily_ThisWeek":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Daily_by_Date(start_Date=${prevMondayyyy}-${prevMondaymm}-${prevMondaydd}T00:00:00.000Z,end_Date=${yyyy}-${mm}-${dd}T23:59:59.000Z)/Set`;
              break;
            case "Daily_ThisMonth":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Daily_by_Date(start_Date=${yyyy}-${mm}-01T00:00:00.000Z,end_Date=${yyyy}-${mm}-${dd}T23:59:59.000Z)/Set`;
              break;
            case "Daily_ThisYear":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Daily_by_Date(start_Date=${yyyy}-01-01T00:00:00.000Z,end_Date=${yyyy}-${mm}-${dd}T23:59:59.000Z)/Set`;
              break;
            case "Weekly_ThisMonth":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Weekly_by_Date(start_Date=${yyyy}-${mm}-01T00:00:00.000Z,end_Date=${yyyy}-${mm}-${dd}T23:59:59.000Z)/Set`;
              break;
            case "Weekly_ThisYear":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Weekly_by_Date(start_Date=${yyyy}-01-01T00:00:00.000Z,end_Date=${yyyy}-${mm}-${dd}T23:59:59.000Z)/Set`;
              break;
            case "Monthly_ThisYear":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Monthly_by_Date(start_Date=${yyyy}-01-01T00:00:00.000Z,end_Date=${yyyy}-${mm}-${dd}T23:59:59.000Z)/Set`;
              break;
            case "Quarterly_ThisYear":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/data/completedata/Complete_Data_Quarterly_by_Date(start_Date=${yyyy}-01-01T00:00:00.000Z,end_Date=${yyyy}-${mm}-${dd}T23:59:59.000Z)/Set`;
              break;
            default:
              alert("Bad scope passed");
          }
        }
        if (measureInput === "Off-Peak Percentage") {
          // Set URL depending on the scope
          switch (scopeInput) {
            case "AllTime_Weekly":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/offpeak/OffPeak_Weekly_Complete(start_Hour=${props.settings.offPeakStart},end_Hour=${props.settings.offPeakEnd})/Set`;
              break;
            case "AllTime_Monthly":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/offpeak/OffPeak_Monthly_Complete(start_Hour=${props.settings.offPeakStart},end_Hour=${props.settings.offPeakEnd})/Set`;
              break;
            case "AllTime_Quarterly":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/offpeak/OffPeak_Quaterly_Complete(start_Hour=${props.settings.offPeakStart},end_Hour=${props.settings.offPeakEnd})/Set`;
              break;
            case "AllTime_Yearly":
              kpiGadgetTemplate.attributes.url = `${hanaRoute}/offpeak/OffPeak_Yearly_Complete(start_Hour=${props.settings.offPeakStart},end_Hour=${props.settings.offPeakEnd})/Set`;
              break;
            default:
              alert("Bad scope passed");
          }
        }
        return props.onAddGadget(kpiGadgetTemplate);
      }
      if (typeInput === "LineChart") {
        return props.onAddGadget({
          id: props.gadgetsCount + 1,
          title: titleInput,
          dashboard: props.dashboardId,
          type: "linechart2",
          startDate: null,
          endDate: null,
          attributes: {
            dimensions: [{ accessor: "timestamp" }],
            measures: [
              {
                accessor: "avg_current (A)",
                color: "red",
              },
              {
                accessor: "max_current (A)",
                color: "blue",
              },
              {
                accessor: "avg_voltage (V)",
                color: "green",
              },
              {
                accessor: "max_voltage (V)",
                color: "purple",
              },
              {
                accessor: "avg_power (kW)",
                color: "orange",
              },
              {
                accessor: "max_power (kW)",
                color: "lime",
              },
              {
                accessor: "avg_cosphi",
                color: "gray",
              },
              {
                accessor: "avg_frequency (Hz)",
                color: "olive",
              },
              {
                accessor: "max_frequency (Hz)",
                color: "pink",
              },
              {
                accessor: "cumulated_pos_consumption",
                color: "navy",
              },
              {
                accessor: "cumulated_neg_consumption",
                color: "black",
              },
            ],
            url: `${hanaRoute}/data/completedata/Complete_Data_Hourly`,
          },
        });
      }
      if (typeInput === "Prediction") {
        return props.onAddGadget({
          id: props.gadgetsCount + 1,
          title: titleInput,
          dashboard: props.dashboardId,
          type: "prediction",
          startDate: null,
          endDate: null,
          attributes: {
            dimensions: [{ accessor: "timestamp" }],
            measures: [
              {
                accessor: "powerconsumption_sensor_1 (kwH)",
                color: "red",
              },
              {
                accessor: "prediction (kwH)",
                color: "blue",
              },
              {
                accessor: "last_year (kwH)",
                color: "green",
              },
            ],
            url: `${hanaRoute}/prediction/Prediction_Mock_Data`,
          },
        });
      }
      if (typeInput === "Text Field") {
        return props.onAddGadget({
          id: props.gadgetsCount + 1,
          title: titleInput,
          dashboard: props.dashboardId,
          type: "markup",
          text: "Type in some text",
          html: { __html: "<p>Type in some text</p>" },
        });
      }
    }
  };

  const handleMeasureChange = (e) => {
    // Switch scopeTypes depending on the measure input, necessary for the conditional rendering of the scopeTypes
    if (e.target._state._text === "Energy Consumption") {
      setScopeTypes(props.scopeTypes.consumption);
    } else {
      setScopeTypes(props.scopeTypes.offpeak);
      setScopeInput(props.scopeTypes.offpeak[0]);
    }
    setMeasureInput(e.target._state._text);
  };

  const DialogHeader = styled.div`
    text-align: left;
    height: 3rem;
    line-height: 3rem;
    color: #32363a;
    font-size: 1rem;
    font-weight: 400;
    font-family: var(--sapFontFamily);
    margin-bootom: 10px;
    border-bottom: 1px solid grey;
    width: 700px;
    display: block;
    margin-bottom: 10px;
  `;

  const FoooterContent = styled.div`
    padding-top: 10px;
    padding-bottom: 10px;
    float: right;
  `;

  const FoooterSubmitButton = styled.button`
    padding: 7px;
    margin-right: 5px;
    margin-left: 5px;
    font-family: var(--sapFontFamily);
    height: 100%;
    font: inherit;
    background-color: #0e62cb;
    color: white;
    border: none;
    border-radius: 4px;
    :hover {
      background-color: #579aed;
      color: white;
    }
  `;

  const FoooterCancelButton = styled.button`
    padding: 7px;
    margin-right: 5px;
    margin-left: 5px;
    border: none;
    background: none;
    font-family: var(--sapFontFamily);
    height: 100%;
    font: inherit;
    color: #0e62cb;
  `;

  return (
    <>
      <a onClick={openDialog}>
        <ui5-icon name="add"></ui5-icon>
      </a>
      <Dialog
        ref={dialogRef}
        footer={
          <FoooterContent>
            <FoooterSubmitButton onClick={handleSubmit}>
              Submit
            </FoooterSubmitButton>
            <FoooterCancelButton onClick={closeDialog}>
              Cancel
            </FoooterCancelButton>
          </FoooterContent>
        }
        header={null}
      >
        <DialogHeader>Create a new Gadget</DialogHeader>
        <Form>
          <FormGroup>
            <FormItem label={"Type"}>
              <Select
                value={props.gadgetTypes}
                onChange={(e) => {
                  setTypeInput(e.target._state._text);
                }}
                multiple
              >
                {props.gadgetTypes
                  .sort((a, b) => {
                    if (a < b) {
                      return 1;
                    }
                    if (a > b) {
                      return -1;
                    }
                    return 0;
                  })
                  .map((gt, i) => (
                    <Option key={i} value={gt}>
                      {gt}
                    </Option>
                  ))}
              </Select>
            </FormItem>
            <FormItem label={"Title"}>
              <Input
                type="text"
                onChange={(e) => setTitleInput(e.target.value)}
              />
            </FormItem>
            {typeInput === "KPI" ? (
              <React.Fragment>
                <FormItem style={{ marginBottom: "6px" }} label={"Measure"}>
                  <Select onChange={handleMeasureChange} multiple>
                    {props.measureTypes.map((mt, i) => (
                      <Option key={i}>{mt}</Option>
                    ))}
                  </Select>
                </FormItem>
                <FormItem style={{ marginBottom: "6px" }} label={"Scope"}>
                  <Select
                    value={props.scopeTypes}
                    onChange={(e) => setScopeInput(e.target._state._text)}
                    multiple
                  >
                    {scopeTypes.map((st, i) => (
                      <Option key={i} value={st}>
                        {st}
                      </Option>
                    ))}
                  </Select>
                </FormItem>
                <FormItem
                  label={"Target (kWh)"}
                  style={{ marginBottom: "6px" }}
                >
                  <Input
                    type={InputType.Number}
                    onChange={(e) => setTargetInput(e.target.value)}
                  />
                </FormItem>
              </React.Fragment>
            ) : (
              <></>
            )}
          </FormGroup>
        </Form>
      </Dialog>
    </>
  );
}

export default AddGadgetPopup;
