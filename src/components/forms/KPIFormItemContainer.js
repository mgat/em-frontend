import React, { Component } from "react";
import {
  FormItem,
  Select,
  Option,
  Input,
  InputType,
} from "@ui5/webcomponents-react/";

class KPIFormItemContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scopeTypes: props.scopeTypes.consumption,
    };
  }

  handleMeasureChange = (e) => {
    // Switch scopeTypes depending on the measure input, necessary for the conditional rendering of the scopeTypes
    if (e.target._state._text === "Energy Consumption") {
      this.setState({ scopeTypes: this.props.scopeTypes.consumption });
    } else {
      this.setState({ scopeTypes: this.props.scopeTypes.offpeak });
      // Bubble up new default scopeType
      this.props.onSelectScope(this.state.scopeTypes[0]);
    }
    this.props.onSelectMeasure(e.target._state._text);
  };

  handleScopeChange = (e) => {
    this.props.onSelectScope(e.target._state._text);
  };

  handleTargetChange = (e) => {
    this.props.onSelectTarget(e.target.value);
  };

  render() {
    return (
      <React.Fragment>
        <FormItem style={{ "margin-bottom": "6px" }} label={"Measure"}>
          <Select onChange={this.handleMeasureChange} multiple>
            {this.props.measureTypes.map((mt, i) => (
              <Option key={i}>{mt}</Option>
            ))}
          </Select>
        </FormItem>
        <FormItem style={{ "margin-bottom": "6px" }} label={"Scope"}>
          <Select
            value={this.props.scopeTypes}
            onChange={this.handleScopeChange}
            multiple
          >
            {this.state.scopeTypes.map((st, i) => (
              <Option key={i} value={st}>
                {st}
              </Option>
            ))}
          </Select>
        </FormItem>
        <FormItem label={"Target (kWh)"} style={{ "margin-bottom": "6px" }}>
          {/** TODO: Check that input is an Int */}
          <Input type={InputType.Number} onChange={this.handleTargetChange} />
        </FormItem>
      </React.Fragment>
    );
  }
}

export default KPIFormItemContainer;
