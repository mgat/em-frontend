import React, { useRef, useCallback } from "react";
import { Dialog, FormItem, Input } from "@ui5/webcomponents-react/";
import { withRouter } from "react-router";
import styled from "styled-components";

function CreateDashboardPopup(props) {
  const dialogRef = useRef();
  const openDialog = useCallback(() => {
    dialogRef.current.open();
  }, [dialogRef]);
  const closeDialog = useCallback(() => {
    dialogRef.current.close();
  }, [dialogRef]);

  const nameInput = useRef(null);

  const handleSubmit = () => {
    const id = props.dashboards.length + 1;
    if (props.onCreateDashboard) {
      props.onCreateDashboard({
        id: props.dashboards.length + 1,
        name: nameInput.current.value,
      });
    }
    closeDialog(id);
  };

  const handleClick = () => {
    handleSubmit();
    const id = props.dashboards.length + 1;
    props.history.push("/" + id);
  };

  const DialogContainer = styled.div``;

  const DialogHeader = styled.div`
    text-align: left;
    height: 3rem;
    line-height: 3rem;
    color: #32363a;
    font-size: 1rem;
    font-weight: 400;
    font-family: var(--sapFontFamily);
    margin-bootom: 10px;
    border-bottom: 1px solid grey;
    width: 500px;
    display: block;
  `;

  const FormContent = styled.div`
    padding: 15px;
  `;

  const FoooterContent = styled.div`
    padding-top: 10px;
    padding-bottom: 10px;
    float: right;
  `;

  const FoooterSubmitButton = styled.button`
    padding: 7px;
    margin-right: 5px;
    margin-left: 5px;
    font-family: var(--sapFontFamily);
    height: 100%;
    font: inherit;
    background-color: #0e62cb;
    color: white;
    border: none;
    border-radius: 4px;
    :hover {
      background-color: #579aed;
      color: white;
    }
  `;

  const FoooterCancelButton = styled.button`
    padding: 7px;
    margin-right: 5px;
    margin-left: 5px;
    border: none;
    background: none;
    font-family: var(--sapFontFamily);
    height: 100%;
    font: inherit;
    color: #0e62cb;
  `;

  return (
    <>
      <ui5-li onClick={openDialog}>Create Dashboard</ui5-li>

      <Dialog
        ref={dialogRef}
        footer={
          <FoooterContent>
            <FoooterSubmitButton onClick={handleClick}>
              Submit
            </FoooterSubmitButton>
            <FoooterCancelButton onClick={closeDialog}>
              Cancel
            </FoooterCancelButton>
          </FoooterContent>
        }
        header={null}
      >
        <DialogContainer>
          <DialogHeader>Create a new Dashboard</DialogHeader>
          <FormContent>
            <FormItem label={"Name"}>
              <Input type="text" ref={nameInput} />
            </FormItem>
          </FormContent>
        </DialogContainer>
      </Dialog>
    </>
  );
}

const CreateDashboardPopupWithRouter = withRouter(CreateDashboardPopup);
export default CreateDashboardPopupWithRouter;
