import React, {Component} from 'react'
import { LineChart } from '@ui5/webcomponents-react-charts/lib/next/LineChart';
import { Button } from '@ui5/webcomponents-react/lib/Button';
import { ButtonDesign } from '@ui5/webcomponents-react/lib/ButtonDesign';
import "@ui5/webcomponents-icons/dist/icons/delete";
import "@ui5/webcomponents-icons/dist/icons/edit";
import "@ui5/webcomponents-icons/dist/icons/save";
import { DatePicker } from '@ui5/webcomponents-react/lib/DatePicker';
import { FormItem, Form } from "@ui5/webcomponents-react/";





class Prediction extends Component{
    constructor(props) {
        super(props);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleStartChange = this.handleStartChange.bind(this);
        this.handleEndChange = this.handleEndChange.bind(this);
        this.state = {
            query: '',
            data: '',
            filteredMeasures: this.props.measures,
            measures: [],
            startDate: this.props.gadget.startDate,
            endDate: this.props.gadget.endDate,
            edit: null
        }
    }

    handleEdit = (e) => {
        this.setState((currentState) => {
            //if change from edit true -> false
            if(currentState.edit){
                this.props.gadget.startDate = currentState.startDate
                this.props.gadget.endDate = currentState.endDate
                this.props.onUpdateGadget(this.props.gadget)
            }
            return ({
                edit: !currentState.edit
            })
        })
    }
    

    handleMeasure = (e) => {
		const measure = e.target.value
        console.log(measure)
		// use react component state over redux state because their is no benefit of putting
        // this form sate into inside of redux and it would be more complicated
        if (this.state.measures.filter((m)=> m === measure).length > 0){
            this.setState((currentState) => ({
                measures: currentState.measures.filter((m)=> m!==measure)
        }))
        }else {
            this.setState((currentState) => ({
                measures: currentState.measures.concat([measure])
        }))
        }
    }
    
    handleStartChange = (e) => {
        const startDate = e.target.value
        console.log(startDate)
        this.setState(()=> ({
            startDate
        }))
    }

    handleEndChange = (e) => {
        const endDate = e.target.value
        console.log(endDate)
        this.setState(()=> ({
            endDate
        }))
    }

    fetchData(){
        var proxyUrl = this.props.proxy,
        targetUrl = this.props.url
        fetch(proxyUrl + targetUrl)
            .then(blob => blob.json())
            .then(data => {
                const entries = Object.entries(data.value)
                // odataset
                const dataset = entries.map(e => {
                            return  {
                                ...e[1],
                                'prediction (kwH)' : e[1].prediction,
                                'powerconsumption_sensor_1 (kwH)' : e[1].powerconsumption_sensor_1,
                                'timestamp' : `${e[1].Date}`
                            }
                        }
                    )
                // get data one year backwards of the lastDayOfPrediction
                const datasetSinceLastYear = dataset.filter((e) => {
                    // last day of static prediction
                    let lastDayOfPrediction = "2020-05-22"
                    let lastYear = new Date(lastDayOfPrediction).getFullYear()-1
                    let lastDayOfPredictionLastYear = new Date(lastDayOfPrediction).setFullYear(lastYear)
                    // if you have livePrediction uncomment 
                    //let lastYear = new Date().getFullYear()-1
                    //let todayLastYear = new Date().setFullYear(lastYear)
                    return (
                        new Date(e.Date) > new Date(lastDayOfPredictionLastYear)
                        )
                    })

                const lastYearForecast = datasetSinceLastYear.map(e => {
                    return {
                        ... e,
                        'Date':`${new Date(e.Date).getFullYear()+1}-${this.getNumberWithLEadingZero(new Date(e.Date).getMonth()+1)}-${this.getNumberWithLEadingZero(new Date(e.Date).getDate())}`,
                        'prediction (kwH)':null,
                        'powerconsumption_sensor_1 (kwH)':null,
                        'last_year (kwH)': e.powerconsumption_sensor_1,
                        'timestamp' : `${new Date(e.Date).getFullYear()+1}-${this.getNumberWithLEadingZero(new Date(e.Date).getMonth()+1)}-${this.getNumberWithLEadingZero(new Date(e.Date).getDate())}`
                    }
                })

                const newDataSet = [... dataset, ... lastYearForecast]
                //console.log("newDataSet",newDataSet)

                this.setState({ data: newDataSet });

                return data.value;
            })
            .catch(e => {
                console.log(e);
                return e;
            });
    }

    getNumberWithLEadingZero(number){
        return `${number+100}`.substring(1)
    }

    

    componentWillMount() {
        this.fetchData()
    }

    getDataSet(){
        if (this.state.startDate && this.state.endDate && this.state.data){
            console.log("START DATE", new Date(this.state.startDate))
            console.log("END DATE", new Date(this.state.endDate))
            const result = this.state.data.filter((e) => new Date(this.state.startDate) <= new Date(e.Date) 
                                    && new Date(e.Date) <= new Date(this.state.endDate))



            {/*.map((e) => {
                                        var obj = obj
                                        if (e.Date){obj = {...obj,'Date' : e.Date}}
                                        if (e.prediction){obj = {...obj,'prediction (kwH)' : e.prediction}} 
                                        if (e.powerconsumption_sensor_1){obj = {...obj,'powerconsumption_sensor_1 (kwH)' : e.powerconsumption_sensor_1}}
                                        if (e.last_year){obj = {...obj,'last_year (kwH)' : e.last_year}}
                                        if (e.timestamp){obj = {...obj,'timestamp' : e.timestamp}}

                                        return obj })*/}
             //console.log("RES",result)
             return result
        } else {
            return this.state.data
        }
    }

    render(){
        return( <div style={{'padding':'16px'}}>
                    {this.state.edit ? <div className='gadget-filter'>
                            <Form>
                                <FormItem label={"Start Date"}>
                                    <DatePicker minDate="Sep 1, 2017" maxDate="May 7, 2021" value={this.state.startDate ? this.state.startDate : ''} onChange={this.handleStartChange}/>
                                </FormItem>
                                <FormItem label={"End Date"}>
                                    <DatePicker minDate="Sep 1, 2017" maxDate="May 7, 2021" value={this.state.endDate ? this.state.endDate : ''} onChange={this.handleEndChange}/>
                                </FormItem>
                            </Form>
                            <Button
                                design={ButtonDesign.Negative}
                                onClick = {()=> this.props.onDeleteGadget(this.props.gadget.id)}
                                icon={"delete"}
                                style={{'margin-right':'10px'}}
                                />
                            <Button
                                design={ButtonDesign.Default}
                                onClick={this.handleEdit}
                                icon={"save"}
                                style={{'margin-right':'10px'}}
                            />
                    </div>
                    :
                    <div style={{'padding':'16px'}}>
                        <LineChart
                            chartConfig = {{'resizeDebounce':3}}
                            dimensions={this.props.dimensions}
                            measures={this.state.filteredMeasures}
                            dataset={this.getDataSet()}
                        />
                        <Button
                            design={ButtonDesign.Negative}
                            onClick = {()=> this.props.onDeleteGadget(this.props.gadget.id)}
                            icon={"delete"}
                            style={{'margin-right':'10px'}}
                        />
                        <Button
                            design={ButtonDesign.Default}
                            onClick={this.handleEdit}
                            icon={"edit"}
                            style={{'margin-right':'10px'}}
                        />
                    </div>}
                </div>
        )
    }
}

export default Prediction