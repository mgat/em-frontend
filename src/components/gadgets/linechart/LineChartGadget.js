import React, {Component} from 'react';
import { LineChart } from '@ui5/webcomponents-react-charts/lib/next/LineChart';
import { Button } from '@ui5/webcomponents-react/lib/Button';
import { ButtonDesign } from '@ui5/webcomponents-react/lib/ButtonDesign';
import { Option } from '@ui5/webcomponents-react/lib/Option';
import { Select } from '@ui5/webcomponents-react/lib/Select';
import { FilterBar } from '@ui5/webcomponents-react/lib/FilterBar';



class LineChartGadget extends Component{

    state = {
        query: '',
        data: '',
        value: 'current'
    }

    handleChange = (e) => {
		const value = e.target.value
        console.log(value)
		// use react component state over redux state because their is no benefit of putting
		// this form sate into inside of redux and it would be more complicated
		this.setState(()=> ({
			value
		}))
    }
    
    handleStartChange = (e) => {
        const value = e.target.value
        console.log(value)
        this.setState()
    }

    handleSubmit = (e) => {
        e.preventDefault()
        const {value} = this.state
        console.log(value)
        this.props.onUpdateGadget(value,this.props.gadget.id)
    }

    componentDidMount() {
    var proxyUrl = 'https://cors-anywhere.herokuapp.com/',
    targetUrl = this.props.url
    fetch(proxyUrl + targetUrl)
        .then(blob => blob.json())
        .then(data => {
            this.setState({ data: data.value });
            //console.table(data.value);

            //use this line for debugging
            //document.querySelector("pre").innerHTML = JSON.stringify(data.value, null, 2);
            return data.value;
        })
        .catch(e => {
            console.log(e);
            return e;
        }); 
  }

    render(){

        const value = this.state.value

        return( <div>
                    <span>{this.props.gadget.title}</span>
                    <div className='gadget-filter'>
                            <form className='filter' onSubmit={this.handleSubmit}>
                                <select value={this.state.value} onChange={this.handleChange}>
                                     {this.props.measures.map((m) => (
                                <option value={m.accessor}>{m.accessor}</option>
                            ))}
                                </select>
                                <label className='btn'>Start Date:</label>
                                    <input className='btn' type='text' onChange={this.handleStartChange}>
                                </input>
                                <button
                                    className='btn'
                                    type='submit'
                                >
                                    Apply Filter
					            </button>
                            </form>
                    </div>
                    <div className='gadget'>
                        <LineChart
                            dimensions={this.props.dimensions}
                            measures={this.props.measures}
                            dataset={this.state.data}
                            style={{ width: '95%', height: '40vh' }}
                        />
                        <Button
                            design={ButtonDesign.Transparent}
                            onClick = {()=> this.props.onDeleteGadget(this.props.gadget.id)}
                        >
                            Remove
                        </Button>
                    </div>
                </div>
        )
    }
}

export default LineChartGadget