import React, { Component } from "react";
import { LineChart } from "@ui5/webcomponents-react-charts/lib/next/LineChart";
import { Button } from "@ui5/webcomponents-react/lib/Button";
import { ButtonDesign } from "@ui5/webcomponents-react/lib/ButtonDesign";
import "@ui5/webcomponents-icons/dist/icons/delete";
import "@ui5/webcomponents-icons/dist/icons/edit";
import "@ui5/webcomponents-icons/dist/icons/save";
import "@ui5/webcomponents-icons/dist/icons/cancel";
import { DatePicker } from "@ui5/webcomponents-react/lib/DatePicker";
import { FormItem, Form, Select, Option } from "@ui5/webcomponents-react/";

class LineChartGadget2 extends Component {
  constructor(props) {
    super(props);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleStartChange = this.handleStartChange.bind(this);
    this.handleEndChange = this.handleEndChange.bind(this);
    this.handleResolution = this.handleResolution.bind(this);
    this.hideCumulation = this.hideCumulation.bind(this);
    this.state = {
      query: "",
      data: "",
      filteredMeasures: this.props.measures,
      measures: [],
      startDate: this.props.gadget.startDate,
      endDate: this.props.gadget.endDate,
      edit: null,
      url: "",
      resolution: "",
      cumulation: true,
    };
  }

  handleEdit = (e) => {
    this.setState((currentState) => {
      //if change from edit true -> false
      if (currentState.edit) {
        this.props.gadget.startDate = currentState.startDate;
        this.props.gadget.endDate = currentState.endDate;
        this.props.gadget.attributes.url = currentState.url;
        console.log("NEW Gadget", this.props.gadget);
        this.props.onUpdateGadget(this.props.gadget);
      }
      return {
        edit: !currentState.edit,
        cumulation: true,
      };
    });
    this.fetchData();
  };

  handleMeasure = (e) => {
    const measure = e.target.value;
    console.log(measure);
    // use react component state over redux state because their is no benefit of putting
    // this form sate into inside of redux and it would be more complicated
    if (this.state.measures.filter((m) => m === measure).length > 0) {
      this.setState((currentState) => ({
        measures: currentState.measures.filter((m) => m !== measure),
      }));
    } else {
      this.setState((currentState) => ({
        measures: currentState.measures.concat([measure]),
      }));
    }
  };

  handleStartChange = (e) => {
    const startDate = e.target.value;
    console.log(startDate);
    this.setState(() => ({
      startDate,
    }));
  };

  handleResolution = (e) => {
    const resolution = e.target.value;
    let url = `${this.props.hanaURL}/data/completedata/Complete_Data_Hourly`;
    resolution === "1"
      ? (url = `${this.props.hanaURL}/data/completedata/Complete_Data_Hourly`)
      : resolution === "2"
      ? (url = `${this.props.hanaURL}/data/completedata/Complete_Data_Daily`)
      : resolution === "3"
      ? (url = `${this.props.hanaURL}/data/completedata/Complete_Data_Monthly`)
      : resolution === "4"
      ? (url = `${this.props.hanaURL}/data/completedata/Complete_Data_Yearly`)
      : console.log("invalid input");

    this.setState(() => ({
      url,
      resolution,
    }));

    console.log(url);
  };

  handleEndChange = (e) => {
    const endDate = e.target.value;
    console.log(endDate);
    this.setState(() => ({
      endDate,
    }));
  };

  hideCumulation = (e) => {
    this.setState((currentState) => ({
      data: currentState.data.map((e) => {
        return {
          ...e,
          cumulated_pos_consumption: currentState.cumulation
            ? null
            : e.hiddePos,
          cumulated_neg_consumption: currentState.cumulation
            ? null
            : e.hiddenNeg,
          hiddePos: !currentState.cumulation
            ? null
            : e.cumulated_pos_consumption,
          hiddenNeg: !currentState.cumulation
            ? null
            : e.cumulated_pos_consumption,
        };
      }),
      cumulation: !currentState.cumulation,
    }));
  };

  fetchData() {
    var proxyUrl = this.props.proxy,
      targetUrl = this.props.url;
    fetch(proxyUrl + targetUrl)
      .then((blob) => blob.json())
      .then((data) => {
        const entries = Object.entries(data.value);
        // odataset
        const dataset = entries.map((e) => {
          return {
            ...e[1],
            "avg_current (A)": e[1].avg_current,
            "max_current (A)": e[1].max_current,
            "avg_voltage (V)": e[1].avg_voltage,
            "max_voltage (V)": e[1].max_voltage,
            "avg_power (kW)": e[1].avg_power,
            "max_power (kW)": e[1].max_power,
            "avg_frequency (Hz)": e.avg_frequency,
            "max_frequency (Hz)": e.max_frequency,
            "cumulated_pos_consumption (kWh)": e.cumulated_pos_consumption,
            "cumulated_neg_consumption (kWh)": e.cumulated_neg_consumption,
            timestamp: `${new Date(
              e[1].year,
              e[1].day ? 0 : e[1].month,
              e[1].day ? e[1].day : 0,
              e[1].hour ? e[1].hour : 0
            )}`,
          };
        });
        console.log(new Date(2020, 0, 5, 0));
        this.setState({ data: dataset });

        return data.value;
      })
      .catch((e) => {
        console.log(e);
        return e;
      });
  }

  getNumberWithLEadingZero(number) {
    return `${number + 100}`.substring(1);
  }

  componentWillMount() {
    this.fetchData();
  }

  getDataSet() {
    if (this.state.startDate && this.state.endDate && this.state.data) {
      console.log("START DATE", new Date(this.state.startDate));
      console.log("END DATE", new Date(this.state.endDate));
      const result = this.state.data.filter((e) => {
        let day = new Date(e.year, 0, e.day);

        return (
          new Date(this.state.startDate) <= new Date(e.timestamp) &&
          new Date(e.timestamp) <= new Date(this.state.endDate)
        );
      });

      {
        /*.map((e) => {
                                        var obj = obj
                                        if (e.Date){obj = {...obj,'Date' : e.Date}}
                                        if (e.prediction){obj = {...obj,'prediction (kwH)' : e.prediction}} 
                                        if (e.powerconsumption_sensor_1){obj = {...obj,'powerconsumption_sensor_1 (kwH)' : e.powerconsumption_sensor_1}}
                                        if (e.last_year){obj = {...obj,'last_year (kwH)' : e.last_year}}
                                        if (e.timestamp){obj = {...obj,'timestamp' : e.timestamp}}

                                        return obj })*/
      }
      console.log("RES", result);
      return result;
    } else {
      return this.state.data;
    }
  }

  render() {
    return (
      <div style={{ padding: "16px" }}>
        {this.state.edit ? (
          <div className="gadget-filter">
            <Form>
              <FormItem key="1" label={"Start Date"}>
                <DatePicker
                  minDate="Jan 1, 2020"
                  maxDate="May 08, 2020"
                  value={this.state.startDate ? this.state.startDate : ""}
                  onChange={this.handleStartChange}
                />
              </FormItem>
              <FormItem key="2" label={"End Date"}>
                <DatePicker
                  minDate="Jan 1, 2020"
                  maxDate="May 08, 2020"
                  value={this.state.endDate ? this.state.endDate : ""}
                  onChange={this.handleEndChange}
                />
              </FormItem>
              <FormItem key="3" label={"Resolution"}>
                <select onChange={this.handleResolution}>
                  <option
                    value="1"
                    selected={this.state.resolution === "1" ? "selected" : ""}
                  >
                    Hourly
                  </option>
                  <option
                    value="2"
                    selected={this.state.resolution === "2" ? "selected" : ""}
                  >
                    Daily
                  </option>
                  <option
                    value="3"
                    selected={this.state.resolution === "3" ? "selected" : ""}
                  >
                    Monthly
                  </option>
                  <option
                    value="4"
                    selected={this.state.resolution === "4" ? "selected" : ""}
                  >
                    Yearly
                  </option>
                </select>
              </FormItem>
            </Form>
            <Button
              design={ButtonDesign.Negative}
              onClick={() => this.props.onDeleteGadget(this.props.gadget.id)}
              icon={"delete"}
              style={{ "margin-right": "10px" }}
            />
            <Button
              design={ButtonDesign.Default}
              onClick={this.handleEdit}
              icon={"save"}
              style={{ "margin-right": "10px" }}
            />
          </div>
        ) : (
          <div style={{ padding: "16px" }}>
            <LineChart
              chartConfig={{ resizeDebounce: 3 }}
              dimensions={this.props.dimensions}
              measures={this.state.filteredMeasures}
              dataset={this.getDataSet()}
            />
            <Button
              design={ButtonDesign.Negative}
              onClick={() => this.props.onDeleteGadget(this.props.gadget.id)}
              icon={"delete"}
              style={{ "margin-right": "10px" }}
            />
            <Button
              design={ButtonDesign.Default}
              onClick={this.handleEdit}
              icon={"edit"}
              style={{ "margin-right": "10px" }}
            />
            <Button
              design={ButtonDesign.Transparent}
              onClick={this.hideCumulation}
              icon={"cancel"}
              style={{ "margin-right": "10px" }}
            >
              {this.state.cumulation ? "Hide" : "Show"} Cumulation
            </Button>
          </div>
        )}
      </div>
    );
  }
}

export default LineChartGadget2;
