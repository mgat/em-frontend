import React, { Component } from "react";
import LineChartGadget from "./LineChartGadget";
import LineChartGadget2 from "./LineChartGadget2";
import Prediction from "./Prediction";
import { Card } from "@ui5/webcomponents-react";
import {
  FlexBox,
  FlexBoxJustifyContent,
  FlexBoxDirection,
} from "@ui5/webcomponents-react";

class LineChartContainer extends Component {
  render() {
    return (
      <ol className="gadget-container">
        {this.props.gadgets
          .filter((g) => g.type === "linechart")
          .map((gadget) => (
            <li id={gadget.id} key={gadget.id}>
              <FlexBox
                key={gadget.id}
                justifyContent={
                  (FlexBoxJustifyContent.Start, FlexBoxDirection.Column)
                }
              >
                <this.CardWrapper>
                  <LineChartGadget
                    dimensions={gadget.attributes.dimensions}
                    measures={gadget.attributes.measures}
                    url={gadget.attributes.url}
                    onDeleteGadget={this.props.onDeleteGadget}
                    onUpdateGadget={this.props.onUpdateGadget}
                    gadget={gadget}
                    type={gadget.type}
                    proxy={this.props.proxy}
                    hanaURL={this.props.hanaURL}
                  />
                </this.CardWrapper>
              </FlexBox>
            </li>
          ))}
        {this.props.gadgets
          .filter((g) => g.type === "linechart2")
          .map((gadget) => (
            <li id={gadget.id} key={gadget.id}>
              <Card heading={gadget.title} className="card">
                <LineChartGadget2
                  dimensions={gadget.attributes.dimensions}
                  measures={gadget.attributes.measures}
                  url={gadget.attributes.url}
                  onDeleteGadget={this.props.onDeleteGadget}
                  onUpdateGadget={this.props.onUpdateGadget}
                  gadget={gadget}
                  proxy={this.props.proxy}
                  hanaURL={this.props.hanaURL}
                />
              </Card>
            </li>
          ))}
        {this.props.gadgets
          .filter((g) => g.type === "prediction")
          .sort((a, b) => {
            if (a.id > b.id) {
              return 1;
            }
            if (a.id < b.id) {
              return -1;
            }
            return 0;
          })
          .map((gadget) => (
            <li id={gadget.id} key={gadget.id}>
              <Card heading={gadget.title} className="card">
                <Prediction
                  dimensions={gadget.attributes.dimensions}
                  measures={gadget.attributes.measures}
                  url={gadget.attributes.url}
                  onDeleteGadget={this.props.onDeleteGadget}
                  onUpdateGadget={this.props.onUpdateGadget}
                  gadget={gadget}
                  proxy={this.props.proxy}
                  hanaURL={this.props.hanaURL}
                />
              </Card>
            </li>
          ))}
      </ol>
    );
  }
}

export default LineChartContainer;
