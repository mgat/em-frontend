import React, { Component } from "react";
import {
  AnalyticalCard,
  AnalyticalCardHeader,
  ValueState,
} from "@ui5/webcomponents-react";
import { Button } from "@ui5/webcomponents-react/lib/Button";
import { ButtonDesign } from "@ui5/webcomponents-react/lib/ButtonDesign";
import { LineChart } from "@ui5/webcomponents-react-charts/";

import "@ui5/webcomponents-icons/dist/icons/delete";
import styled from "styled-components";

class KPIGadget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          avg_current: 0,
          max_current: 0,
          avg_voltage: 0,
          max_voltage: 0,
          avg_power: 0,
          max_power: 0,
          avg_cosphi: 0,
          max_cosphi: 0,
          avg_frequency: 0,
          max_frequency: 0,
          cumulated_pos_consumption: 0,
          cumulated_neg_consumption: 0,
          year: 0,
          quarter: 0,
          month: 0,
          week: 0,
          day: 0,
          hour: 0,
        },
      ],
      offPeakData: [
        {
          avg_current: 0,
          max_current: 0,
          avg_voltage: 0,
          max_voltage: 0,
          avg_power: 0,
          max_power: 0,
          avg_cosphi: 0,
          max_cosphi: 0,
          avg_frequency: 0,
          max_frequency: 0,
          cumulated_pos_consumption: 0,
          cumulated_neg_consumption: 0,
          year: 0,
          quarter: 0,
          month: 0,
          week: 0,
        },
      ],
      value: 0,
      valueState: ValueState.Success,
      lineChartDatasets: [],
      lineChartLabels: [],
    };
  }

  ButtonWrapper = styled.div`
    padding: 4px;
  `;

  componentDidMount() {
    const proxyUrl = this.props.proxy;
    const hanaRoute = this.props.hanaURL;
    const targetUrl = this.props.gadget.attributes.url;
    console.log(targetUrl);

    fetch(proxyUrl + targetUrl)
      .then((blob) => blob.json())
      .then((data) => {
        this.setState({ data: data.value });
        if (this.props.gadget.attributes.measure === "Energy Consumption") {
          this.calculateAvgConsumption();
        }
        if (this.props.gadget.attributes.measure === "Off-Peak Percentage") {
          // Fetch a second dataset to be able to calculate a percentage
          let targetUrl2 = "";
          switch (this.props.gadget.attributes.scope) {
            case "AllTime_Weekly":
              targetUrl2 = `${hanaRoute}/data/completedata/Complete_Data_Weekly`;
              break;
            case "AllTime_Monthly":
              targetUrl2 = `${hanaRoute}/data/completedata/Complete_Data_Monthly`;
              break;
            case "AllTime_Quarterly":
              targetUrl2 = `${hanaRoute}/data/completedata/Complete_Data_Quaterly`;
              break;
            case "AllTime_Yearly":
              targetUrl2 = `${hanaRoute}/data/completedata/Complete_Data_Yearly`;
              break;
            default:
              alert("Unknown scope passed");
          }
          fetch(proxyUrl + targetUrl2)
            .then((blob) => blob.json())
            .then((data) => {
              this.setState({ offPeakData: data.value });
              this.calculateOffPeakPercentage();
            })
            .catch((e) => {
              console.log(e);
              return e;
            });
        }
        //use this line for debugging
        //document.querySelector("pre").innerHTML = JSON.stringify(data.value, null, 2);
        //console.log(this.state.data.map(el => ({cumulated_pos_consumption: el.cumulated_pos_consumption})).reduce((acc, item) => {return acc + item.cumulated_pos_consumption}, 0))
        return data.value;
      })
      .catch((e) => {
        console.log(e);
        return e;
      });
  }

  calculateAvgConsumption = () => {
    // Retrieve only cumulated_pos_consumption fields, add the numbers together, divide by 1000 (to reach kWh), divide by the length to get an avg., and round
    this.setState(
      {
        value: (
          this.state.data
            .map((el) => ({
              cumulated_pos_consumption: el.cumulated_pos_consumption,
            }))
            .reduce((acc, item) => {
              return acc + item.cumulated_pos_consumption;
            }, 0) /
          1000 /
          this.state.data.length
        ).toFixed(1),
      },
      () => {
        if (Number(this.state.value) > Number(this.props.gadget.target)) {
          this.setState({ valueState: ValueState.Warning });
        }
      }
    );
    const lineChartData = this.state.data
      .slice(Math.max(this.state.data.length - 10, 0))
      .map((entry) => {
        return {
          cumulated_pos_consumption: (
            entry.cumulated_pos_consumption / 1000
          ).toFixed(1),
          timestamp: this.getTimestamp(entry),
        };
      });
    console.log(lineChartData);
    this.setState(
      {
        lineChartDatasets: [
          {
            label: `${this.props.gadget.attributes.scope} ${this.props.gadget.attributes.measure} (kWh)`,
            data: lineChartData.map((entry) => entry.cumulated_pos_consumption),
          },
        ],
        lineChartLabels: lineChartData.map((entry) => entry.timestamp),
      },
      () => console.log(this.state)
    );
  };

  calculateOffPeakPercentage = () => {
    // Retrieve cumulated_pos_consumption fields for the whole day and for the off-peak time (includes weekends)
    const wholeDayConsumption = this.state.offPeakData.map(
      (entry) => entry.cumulated_pos_consumption
    );
    const offPeakConsumption = this.state.data.map(
      (entry) => entry.cumulated_pos_consumption
    );
    let percentageArray = offPeakConsumption.map(function (n, i) {
      return n / wholeDayConsumption[i];
    });

    if (wholeDayConsumption.length === offPeakConsumption.length) {
      this.setState(
        {
          value: (
            (percentageArray.reduce((a, b) => a + b, 0) /
              percentageArray.length) *
            100
          ).toFixed(1),
        },
        () => {
          if (Number(this.state.value) < Number(this.props.gadget.target)) {
            this.setState({ valueState: ValueState.Warning });
          }
        }
      );
      percentageArray = percentageArray.map((entry) =>
        (entry * 100).toFixed(2)
      );
      // Data for the linechart
      const timestamps = this.state.data
        .slice(Math.max(this.state.data.length - 10, 0))
        .map((entry) => this.getTimestamp(entry));

      console.log(timestamps);
      console.log(percentageArray);

      const lineChartData = {
        offPeakPercentage: percentageArray.slice(
          Math.max(this.state.data.length - 10, 0)
        ),
        timestamp: timestamps,
      };

      this.setState(
        {
          lineChartDatasets: [
            {
              label: `${this.props.gadget.attributes.scope} ${this.props.gadget.attributes.measure} (%)`,
              data: lineChartData.offPeakPercentage,
            },
          ],
          lineChartLabels: lineChartData.timestamp,
        },
        () => console.log(this.state)
      );
    } else {
      throw new Error("Wrong datasets compared");
    }
  };

  getTimestamp = (data) => {
    const date = new Date(data.year, 0); // initialize a date in `year-01-01`
    // Convert the day of the year (day column) to an actual date
    if (data.month != null) {
      if (data.day != null) {
        let timestamp = new Date(date.setDate(data.day)); // add the number of days
        if (data.hour != null) {
          timestamp.setHours(data.hour);
          return (
            timestamp.getFullYear() +
            "-" +
            (timestamp.getMonth() + 1) +
            "-" +
            timestamp.getDate() +
            " h:" +
            timestamp.getHours()
          );
        } else {
          return (
            timestamp.getFullYear() +
            "-" +
            (timestamp.getMonth() + 1) +
            "-" +
            timestamp.getDate()
          );
        }
      } else {
        return data.year + "-" + data.month;
      }
    } else {
      return data.year;
    }
  };

  render() {
    return (
      <>
        <AnalyticalCard
          width={"20rem"}
          header={
            <AnalyticalCardHeader
              showIndicator=""
              title={this.props.gadget.title}
              value={this.state.value}
              unit={
                this.props.gadget.attributes.measure === "Energy Consumption"
                  ? "kWh"
                  : "%"
              }
              valueState={this.state.valueState}
              target={this.props.gadget.target}
              deviation={this.state.deviation}
            />
          }
        >
          <LineChart
            datasets={this.state.lineChartDatasets}
            labels={this.state.lineChartLabels}
          />
          <Button
            design={ButtonDesign.Negative}
            onClick={() => this.props.onDeleteGadget(this.props.gadget.id)}
            icon={"delete"}
          />
        </AnalyticalCard>
      </>
    );
  }
}

export default KPIGadget;
