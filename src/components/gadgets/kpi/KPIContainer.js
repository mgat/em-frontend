import React, { Component } from "react";
import KPIGadget from "./KPIGadget";
import {
  FlexBox,
  FlexBoxJustifyContent,
  FlexBoxWrap,
} from "@ui5/webcomponents-react";
import styled from "styled-components";

class KPIContainer extends Component {
  CardWrapper = styled.div`
    margin-right: 10px;
    margin-left: 10px;
    margin-bottom: 10px;
    padding: 4px;
  `;
  render() {
    return (
      <FlexBox
        justifyContent={FlexBoxJustifyContent.Start}
        wrap={FlexBoxWrap.Wrap}
      >
        {this.props.gadgets.map((gadget) => (
          <this.CardWrapper>
            <KPIGadget
              key={gadget.id}
              onDeleteGadget={this.props.onDeleteGadget}
              gadget={gadget}
              settings={this.props.settings}
              proxy={this.props.proxy}
              hanaURL={this.props.hanaURL}
            />
          </this.CardWrapper>
        ))}
      </FlexBox>
    );
  }
}

export default KPIContainer;
