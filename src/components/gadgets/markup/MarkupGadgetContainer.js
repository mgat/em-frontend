import React, { Component } from "react";
import MarkupGadget from "./MarkupGadget";
import { Card } from "@ui5/webcomponents-react";

class MarkupGadgetContainer extends Component {
  render() {
    return (
      <ol className="gadget-container">
        {this.props.gadgets
          .filter((g) => g.type === "markup")
          .map((gadget) => (
            <li id={gadget.id} key={gadget.id}>
              <Card heading={gadget.title} className="card">
                <MarkupGadget
                  onDeleteGadget={this.props.onDeleteGadget}
                  onUpdateGadget={this.props.onUpdateGadget}
                  gadget={gadget}
                  proxy={this.props.proxy}
                />
              </Card>
            </li>
          ))}
      </ol>
    );
  }
}

export default MarkupGadgetContainer;
