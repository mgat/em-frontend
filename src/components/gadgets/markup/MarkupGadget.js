import React, {Component} from 'react'
import { LineChart } from '@ui5/webcomponents-react-charts/lib/next/LineChart';
import { Button } from '@ui5/webcomponents-react/lib/Button';
import { ButtonDesign } from '@ui5/webcomponents-react/lib/ButtonDesign';
import "@ui5/webcomponents-icons/dist/icons/delete";
import "@ui5/webcomponents-icons/dist/icons/edit";
import "@ui5/webcomponents-icons/dist/icons/save";

import { Remarkable } from 'remarkable';




class MarkupGadget extends Component{
    constructor(props) {
        super(props);
        this.md = new Remarkable();
        this.handleChange = this.handleChange.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.state = {
            query: '',
            data: '',
            filteredData: '',
            filteredMeasures: this.props.measures,
            measures: [],
            startDate: '1',
            endDate: '42',
            edit: null,
            value: this.props.gadget.text,
            html: this.props.gadget.html
        }
    }

    handleEdit = (e) => {
        this.setState((currentState) => {
            let test = ''
            if(currentState.edit){
                this.props.gadget.text = currentState.value
                this.props.gadget.html = this.getRawMarkup()
                this.props.onUpdateGadget(this.props.gadget)
            }
            return ({
                edit: !currentState.edit,
                html: this.getRawMarkup()
            })
        })
    }

     handleChange(e) {
        this.setState({ value: e.target.value });
     }

     getRawMarkup() {
        return { __html: this.md.render(this.state.value) };
      }

    fetchData(){
        var proxyUrl = this.props.proxy,
        targetUrl = this.props.url
        fetch(proxyUrl + targetUrl)
            .then(blob => blob.json())
            .then(data => {
                const entries = Object.entries(data.value)
                console.log(entries)
                //console.table(data.value);

                //use this line for debugging
                //document.querySelector("pre").innerHTML = JSON.stringify(data.value, null, 2);
                return data.value;
            })
            .catch(e => {
                console.log(e);
                return e;
            });
    }

    componentDidMount() {
    
    //this.fetchData()
       // this.setState({ filteredMeasures: this.props.measures });
  }

    render(){
        return( <div style={{'padding':'16px'}}>
                  {this.state.edit ? 
                    <div>
                      <div className="MarkdownEditor" style={{'display': 'flex'}}>
                        <div style={{'padding':'16px'}}>
                        <h3>Input</h3>
                        <textarea
                          id="markdown-content"
                          onChange={this.handleChange}
                          defaultValue={this.state.value}
                        />
                        </div>
                        <div style={{'padding':'16px'}}>
                        <h3>Output</h3>
                        <div
                          className="content"
                          dangerouslySetInnerHTML={this.getRawMarkup()}
                        />
                        </div>
                      </div>
                    <Button
                            design={ButtonDesign.Negative}
                            onClick = {()=> this.props.onDeleteGadget(this.props.gadget.id)}
                            icon={"delete"}
                            style={{'margin-right':'10px'}}
                    />
                    <Button
                                design={ButtonDesign.Default}
                                onClick={this.handleEdit}
                                icon={"save"}
                                style={{'margin-right':'10px'}}
                            />
                    </div>
                    
                :
                    <div>
                        <div style={{'padding':'16px'}}>
                        <div
                          className="content"
                          dangerouslySetInnerHTML={this.state.html}
                        />
                        </div>
                    <Button
                            design={ButtonDesign.Negative}
                            onClick = {()=> this.props.onDeleteGadget(this.props.gadget.id)}
                            icon={"delete"}
                            style={{'margin-right':'10px'}}
                    />
                    <Button
                                design={ButtonDesign.Default}
                                onClick={this.handleEdit}
                                icon={"edit"}
                                style={{'margin-right':'10px'}}
                            />
                    </div>} 
                </div>
                
        )
    }
}

export default MarkupGadget