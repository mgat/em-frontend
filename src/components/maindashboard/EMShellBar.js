import React, { useRef, useCallback, useState } from "react";
import { Link, Redirect } from "react-router-dom";
import {
  ShellBar,
  ShellBarItem,
  Avatar,
  Popover,
  Card,
  List,
  StandardListItem,
} from "@ui5/webcomponents-react";
import "@ui5/webcomponents-icons/dist/icons/employee";
import "@ui5/webcomponents-icons/dist/icons/settings";
import "@ui5/webcomponents-icons/dist/icons/alert";

function EMShellBar(props) {
  const [redirect, setRedirect] = useState(null);
  const popoverRef = useRef();
  const onButtonClick = useCallback(
    (e) => {
      popoverRef.current.openBy(e.target);
    },
    [popoverRef]
  );

  const onHeaderClick = () => {
    setRedirect("/events");
  };

  return (
    <>
      {redirect ? <Redirect push to={redirect} /> : <></>}
      <ShellBar
        primaryTitle="Dashboards"
        secondaryTitle="ENERGY MANAGER"
        notificationCount={props.notificationCounter}
        logo="./cg_white.png"
        onLogoClick={() => setRedirect("/systemDashboard")}
        style={{ backgroundColor: "black" }}
        menuItems={
          <>
            <Link to="/">
              <ui5-li data-key="1">Main Dashboard</ui5-li>
            </Link>
            {props.dashboards ? (
              props.dashboards.map((db) => (
                <Link key={db.id} to={"/" + db.id}>
                  <ui5-li>{db.name}</ui5-li>
                </Link>
              ))
            ) : (
              <p />
            )}
          </>
        }
        showNotifications={true}
        onNotificationsClick={onButtonClick}
        profile={<Avatar icon="employee" size="XS" />}
        className="fixed-top"
      >
        <ShellBarItem
          icon="settings"
          onItemClick={() => setRedirect("/settings")}
        />
      </ShellBar>
      <Popover
        className="popover"
        ref={popoverRef}
        noArrow={true}
        placement-type="Bottom"
        horizontalAlign="Right"
      >
        <Card
          headerInteractive={true}
          heading="Events"
          avatar={<Avatar icon="alert" />}
          onHeaderClick={onHeaderClick}
          style={{ width: "300px" }}
        >
          <List>
            {props.newNotifications ? (
              props.newNotifications.map((newNotification, i) => {
                console.log("Notification in ShellBar: ", newNotification);
                return (
                  <StandardListItem
                    key={i}
                    description={`${newNotification.date.getFullYear()}-${
                      newNotification.date.getMonth() + 1
                    }-${newNotification.date.getDate()}`}
                  >
                    Target exceeded by more than {newNotification.exceedTarget}{" "}
                    % for daily average consumption.
                  </StandardListItem>
                );
              })
            ) : (
              <></>
            )}
          </List>
        </Card>
      </Popover>
    </>
  );
}

export default EMShellBar;
