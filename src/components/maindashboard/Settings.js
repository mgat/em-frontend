import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import {
  Form,
  FormGroup,
  FormItem,
  TimePicker,
  Button,
  Input,
  InputType,
  FlexBox,
  FlexBoxJustifyContent,
  ButtonDesign,
} from "@ui5/webcomponents-react/";
import styled from "styled-components";

const CustomTitle = styled.div`
  font-family: var(--sapFontFamily);
  font-size: var(--sapFontSize);
  font-size: 30px;
  padding: 10px 0;
`;

const Action = styled.div`
  box-sizing: border-box;
  display: inline-block;
  border-radius: 3px;
  text-decoration: none;
  font-family: var(--sapFontFamily);
  font-size: var(--sapFontSize);
  background-color: #f4f5f7;
  margin-right: 10px;
  padding: 10px;
  font-size: 21px;
`;

const ButtonWrapper = styled.div`
  margin-right: 10px;
  margin-left: 10px;
`;

function Settings(props) {
  let settings = props.settings;

  // Used for navigation
  const [redirect, setRedirect] = useState(null);

  const convertTime12to24 = (time12h) => {
    let [time, modifier] = time12h.split(" ");

    if (time === "12") {
      time = "00";
    }

    if (modifier === "PM") {
      time = parseInt(time, 10) + 12;
    }

    return `${time}`;
  };

  const convertTime24to12 = (time24h) => {
    const AmOrPm = time24h >= 12 ? "PM" : "AM";
    const hours = time24h % 12 || 12;
    return hours + " " + AmOrPm;
  };

  const handleOffPeakStartChange = (e) => {
    let time = e.target.value;
    settings.offPeakStart = convertTime12to24(time);
  };

  const handleOffPeakEndChange = (e) => {
    let time = e.target.value;
    settings.offPeakEnd = convertTime12to24(time);
  };

  const handleExceedTargetChange = (e) => {
    settings.exceedTarget = parseInt(e.target.value);
  };

  const handleSubmit = () => {
    props.onChangeSettings(settings);
    setRedirect("/");
  };

  // TODO: Add Building --> add Floor --> add Room --> add Equipment --> assign ID (fetch these IDs from the database)
  if (redirect) {
    return <Redirect push to={redirect} />;
  }

  return (
    <div className="dashboard-container">
      <CustomTitle>Settings</CustomTitle>
      <Action>
        <Form>
          <FormGroup title={"Off-Peak hours"}>
            <FormItem label={"Work starts at "}>
              <TimePicker
                formatPattern="hh a"
                value={convertTime24to12(settings.offPeakStart)}
                onChange={handleOffPeakStartChange}
              />
            </FormItem>
            <FormItem label={"Work ends at "}>
              <TimePicker
                formatPattern="hh a"
                value={convertTime24to12(settings.offPeakEnd)}
                onChange={handleOffPeakEndChange}
              />
            </FormItem>
          </FormGroup>
          <FormGroup title={"Targets (Avg. daily consumption in kWh)"}>
            <FormItem label={"Line 1"}>
              <Input
                type={InputType.Number}
                onChange={(e) =>
                  (settings.line1Target = parseInt(e.target.value))
                }
                value={settings.line1Target}
              />
            </FormItem>
            <FormItem label={"Line 2"}>
              <Input
                type={InputType.Number}
                onChange={(e) =>
                  (settings.line2Target = parseInt(e.target.value))
                }
                value={settings.line2Target}
              />
            </FormItem>
            <FormItem label={"Line 3"}>
              <Input
                type={InputType.Number}
                onChange={(e) =>
                  (settings.line3Target = parseInt(e.target.value))
                }
                value={settings.line3Target}
              />
            </FormItem>
            <FormItem label={"All lines"}>
              <Input
                type={InputType.Number}
                onChange={(e) =>
                  (settings.allLinesTarget = parseInt(e.target.value))
                }
                value={settings.allLinesTarget}
              />
            </FormItem>
          </FormGroup>
          <FormGroup title={"Trigger notifications"}>
            <FormItem label={"Exceeding target by (%)"}>
              <Input
                type={InputType.Number}
                onChange={handleExceedTargetChange}
                value={settings.exceedTarget}
              />
            </FormItem>
          </FormGroup>
        </Form>
        <FlexBox justifyContent={FlexBoxJustifyContent.End}>
          <Button design={ButtonDesign.Positive} onClick={handleSubmit}>
            Submit
          </Button>
          <ButtonWrapper>
            <Button
              design={ButtonDesign.Negative}
              onClick={() => setRedirect("/")}
            >
              Cancel
            </Button>
          </ButtonWrapper>
        </FlexBox>
      </Action>
    </div>
  );
}

export default Settings;
