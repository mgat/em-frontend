import React, { useRef, useCallback } from "react";
import { Popover } from "@ui5/webcomponents-react";
import Dashboard from "../dashboard/Dashboard";
import CreateDashboardPopup from "../forms/CreateDashboardPopup";
import styled from "styled-components";
import { BsThreeDots } from "react-icons/bs";
import Timeline from "./Timeline";
import AddGadgetPopup from "../forms/AddGadgetPopup";
import "@ui5/webcomponents-icons/dist/icons/add";
import { Button } from "@ui5/webcomponents-react/lib/Button";

const PlaceholderContainer = styled.div`
  background: transparent
    url(//d2lc6wg1utpe39.cloudfront.net/atl-vertigo--shard-jira-prod-eu-15--2--jres.atlassian.net/s/-egccmf/b/23/aac02e2…/_/download/resources/com.atlassian.jira.jira-atlaskit-plugin:overrides-dashboard/images/empty-text.svg)
    50px 50% no-repeat;
  background-size: 120px;
  padding: 80px 0 80px calc(120px + 50px * 2);
  font-family: var(--sapFontFamily);
  font-size: var(--sapFontSize);
  margin-left: 25px;
  min-height: initial;
  text-align: left;
  border: 2px dashed #ccc;
  -moz-border-radius: 3px;
  border-radius: 3px;
  color: #aaa;
`;

const Action = styled.div`
  box-sizing: border-box;
  display: inline-block;
  border-radius: 3px;
  text-decoration: none;
  font-family: var(--sapFontFamily);
  font-size: var(--sapFontSize);
  text-align: center;
  background-color: #f4f5f7;
  margin-left: 10px;
  margin-right: 10px;
  padding: 4px;
  font-size: 21px;
`;

function Placeholder(props) {
  return (
    <PlaceholderContainer>
      <a>Add a new gadget.</a>
      <Action className="action">
        <AddGadgetPopup
          onAddGadget={props.onAddGadget}
          gadgets={props.gadgets}
          gadgetsCount={props.gadgetsCount}
          gadgetTypes={["Prediction", "LineChart", "KPI", "Text Field"]}
          measureTypes={["Energy Consumption", "Off-Peak Percentage"]}
          scopeTypes={{
            consumption: [
              "AllTime_Hourly",
              "AllTime_Daily",
              "AllTime_Weekly",
              "AllTime_Monthly",
              "AllTime_Quarterly",
              "AllTime_Yearly",
              "Hourly_ThisWeek",
              "Hourly_ThisMonth",
              "Hourly_ThisYear",
              "Daily_ThisWeek",
              "Daily_ThisMonth",
              "Daily_ThisYear",
              "Weekly_ThisMonth",
              "Weekly_ThisYear",
              "Monthly_ThisYear",
              "Quarterly_ThisYear",
            ],
            offpeak: [
              "AllTime_Weekly",
              "AllTime_Monthly",
              "AllTime_Quarterly",
              "AllTime_Yearly",
            ],
          }}
          dashboardId={props.dashboardId}
          settings={props.settings}
          hanaURL={props.hanaURL}
        />
      </Action>
    </PlaceholderContainer>
  );
}

export default Placeholder;
