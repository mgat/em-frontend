import React from "react";
import {
  Notification,
  Avatar,
  AvatarSize,
  AvatarShape,
} from "@ui5/webcomponents-react/";
import styled from "styled-components";

const CustomTitle = styled.div`
  font-family: var(--sapFontFamily);
  font-size: var(--sapFontSize);
  font-size: 30px;
  padding: 10px 0;
`;

const Subtitle = styled.div`
  font-family: var(--sapFontFamily);
  font-size: var(--sapFontSize);
  font-size: 21px;
  padding: 10px 0;
`;

class Events extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    console.log("newNotifications", this.props.newNotifications);
    console.log("events", this.props.events);
    return (
      <div className="dashboard-container">
        <CustomTitle>Events</CustomTitle>
        <hr />
        <Subtitle>New Events</Subtitle>
        {this.props.newNotifications.map((newNotification, i) => {
          return (
            <Notification
              key={i}
              description={`Target for avg. daily consumption exceeded by more than ${
                this.props.settings.exceedTarget
              }%: ${(newNotification.cumulated_pos_consumption / 1000).toFixed(
                2
              )}kWh`}
              title={`Event - Equipment ${newNotification.equipment_ID}`}
              datetime={`${newNotification.date.getFullYear()}-${
                newNotification.date.getMonth() + 1
              }-${newNotification.date.getDate()}`}
              avatar={
                <Avatar
                  size={AvatarSize.XS}
                  shape={AvatarShape.Circle}
                  icon="alert"
                  backgroundColor="Accent2"
                />
              }
              noShowMoreButton={true}
              noCloseButton={true}
              style={{ marginBottom: "20px" }}
            />
          );
        })}
        <Subtitle style={{ marginTop: "20px" }}>Old Events</Subtitle>
        {this.props.events.map((event, i) => {
          return (
            <Notification
              key={i}
              description={`Target for avg. daily consumption exceeded by more than ${
                this.props.settings.exceedTarget
              }%: ${(event.cumulated_pos_consumption / 1000).toFixed(2)}kWh`}
              title={`Event - Equipment ${event.equipment_ID}`}
              datetime={`${event.date.getFullYear()}-${
                event.date.getMonth() + 1
              }-${event.date.getDate()}`}
              avatar={
                <Avatar
                  size={AvatarSize.XS}
                  shape={AvatarShape.Circle}
                  icon="alert"
                  backgroundColor="Accent1"
                />
              }
              noShowMoreButton={true}
              noCloseButton={true}
              style={{ marginBottom: "20px" }}
            />
          );
        })}
      </div>
    );
  }
}

export default Events;
