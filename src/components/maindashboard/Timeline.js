import React, { Component } from "react";
import {
  Select,
  Option,
  Table,
  TableColumn,
  Label,
  TableRow,
  TableCell,
  Button,
  MultiComboBox,
  MultiComboBoxItem,
  FlexBox,
  FlexBoxJustifyContent,
  FlexBoxAlignItems,
} from "@ui5/webcomponents-react";
import styled from "styled-components";
import { LineChart } from "@ui5/webcomponents-react-charts/lib/next/LineChart";
import completeHourly20 from "../../utils/completeHourly20.json";
import completeHourly from "../../utils/completeHourly.json";
import completeDaily from "../../utils/completeDaily.json";
import equipmentHourly from "../../utils/equipmentHourly.json";
import equipmentDaily from "../../utils/equipmentDaily.json";

class Timeline extends Component {
  state = {
    equipmentDataHourly: [],
    equipmentDataDaily: [],
    allDataHourly: [],
    allDataDaily: [],
    currentDataSet: [],
    chosenEquipment: 0,
    chosenScope: "hourly",
    chosenMeasures: [
      {
        accessor: "avg_power",
        color: "red",
      },
      {
        accessor: "avg_current",
        color: "blue",
      },
      {
        accessor: "cumulated_pos_consumption",
        color: "orange",
      },
    ],
    allDataShown: false,
    currentTarget: null,
    showTarget: true,
  };

  FormInputWrapper = styled.div`
    margin-right: 10px;
    margin-left: 10px;
    margin-bottom: 10px;
    padding: 4px;
  `;

  hanaUrl = this.props.hanaURL;
  proxyUrl = this.props.proxy;

  useLocalData = false;

  fetchData = (count) => {
    if (this.useLocalData) {
      // Fetch data for all equipments combined
      let data = completeHourly;
      // Convert W and Wh to kW and kWh
      data = data.map((entry) => this.wToKW(entry));
      // Round values
      data = data.map((entry) => this.roundObject(entry));
      // Convert year, month, day, and hour to proper timestamp
      data = data.map((entry) => this.createTimestamp(entry));
      this.setState({
        allDataHourly: data,
      });

      data = completeDaily;
      // Convert W and Wh to kW and kWh
      data = data.map((entry) => this.wToKW(entry));
      // Round values
      data = data.map((entry) => this.roundObject(entry));
      // Convert year, month, day, and hour to proper timestamp
      data = data.map((entry) => this.createTimestamp(entry));
      this.setState({
        allDataDaily: data,
      });

      // Fetch the data for each of the equipments
      let equipmentObjHourly = [];
      let equipmentObjDaily = [];

      for (let i = 1; i <= 3; i++) {
        data = equipmentHourly[i - 1];
        data = data.map((entry) => this.wToKW(entry));
        // Round values
        data = data.map((entry) => this.roundObject(entry));
        // Convert year, month, day, and hour to proper timestamp
        data = data.map((entry) => this.createTimestamp(entry));
        equipmentObjHourly.push({
          equipment: { id: i, data: { data } },
        });

        data = equipmentDaily[i - 1];
        data = data.map((entry) => this.wToKW(entry));
        // Round values
        data = data.map((entry) => this.roundObject(entry));
        // Convert year, month, day, and hour to proper timestamp
        data = data.map((entry) => this.createTimestamp(entry));
        equipmentObjDaily.push({
          equipment: { id: i, data: { data } },
        });
        this.setState({
          equipmentDataHourly: equipmentObjHourly,
        });

        this.setState({
          equipmentDataDaily: equipmentObjDaily,
        });
      }
    } else {
      let targetUrlHourly = "";
      let targetUrlDaily = "";

      // Fetch data for all equipments combined
      if (count === 0) {
        targetUrlHourly = `${this.hanaUrl}/data/completedata/Complete_Data_Hourly`;
        targetUrlDaily = `${this.hanaUrl}/data/completedata/Complete_Data_Daily`;
      } else {
        targetUrlHourly = `${this.hanaUrl}/data/completedata/Complete_Data_Hourly?$top=${count}`;
        targetUrlDaily = `${this.hanaUrl}/data/completedata/Complete_Data_Hourly?$top=${count}`;
      }

      fetch(this.proxyUrl + targetUrlHourly)
        .then((blob) => blob.json())
        .then((data) => {
          // Convert W and Wh to kW and kWh
          data.value = data.value.map((entry) => this.wToKW(entry));
          // Round values
          data.value = data.value.map((entry) => this.roundObject(entry));
          // Convert year, month, day, and hour to proper timestamp
          data.value = data.value.map((entry) => this.createTimestamp(entry));

          this.setState({
            allDataHourly: data.value,
          });
        });

      fetch(this.proxyUrl + targetUrlDaily)
        .then((blob) => blob.json())
        .then((data) => {
          // Convert W and Wh to kW and kWh
          data.value = data.value.map((entry) => this.wToKW(entry));
          // Round values
          data.value = data.value.map((entry) => this.roundObject(entry));
          // Convert year, month, day, and hour to proper timestamp
          data.value = data.value.map((entry) => this.createTimestamp(entry));

          this.setState({
            allDataDaily: data.value,
          });
        });

      // Fetch the data for each of the equipments
      let equipmentObjHourly = [];
      let equipmentObjDaily = [];
      const equipmentCount = 3;
      // Set up a dummy object for each device, these can then be filled asynchronously
      for (let i = 1; i <= equipmentCount; i++) {
        // count === 0 loads all data
        if (count === 0) {
          targetUrlHourly = `${this.hanaUrl}/data/equipment/Equipments_Hourly_by_ID(id=${i})/Set`;
          targetUrlDaily = `${this.hanaUrl}/data/equipment/Equipments_Daily_by_ID(id=${i})/Set`;
        } else {
          targetUrlHourly = `${this.hanaUrl}/data/equipment/Equipments_Hourly_by_ID(id=${i})/Set?$top=${count}`;
          targetUrlDaily = `${this.hanaUrl}/data/equipment/Equipments_Daily_by_ID(id=${i})/Set?$top=${count}`;
        }
        equipmentObjHourly.push({
          equipment: { id: i, data: {}, url: targetUrlHourly },
        });
        equipmentObjDaily.push({
          equipment: { id: i, data: {}, url: targetUrlDaily },
        });
      }
      const equipmentObjCopyHourly = [...equipmentObjHourly];
      const equipmentObjCopyDaily = [...equipmentObjDaily];
      equipmentObjHourly.forEach((element, index) => {
        fetch(this.proxyUrl + element.equipment.url)
          .then((blob) => blob.json())
          .then((data) => {
            equipmentObjCopyHourly[index].equipment.data = data.value;
            // Convert W and Wh to kW and kWh
            equipmentObjCopyHourly[
              index
            ].equipment.data = equipmentObjCopyHourly[
              index
            ].equipment.data.map((entry) => this.wToKW(entry));
            // Round numbers
            equipmentObjCopyHourly[
              index
            ].equipment.data = equipmentObjCopyHourly[
              index
            ].equipment.data.map((entry) => this.roundObject(entry));
            // Convert year, month, day, and hour to proper timestamp
            equipmentObjCopyHourly[
              index
            ].equipment.data = equipmentObjCopyHourly[
              index
            ].equipment.data.map((entry) => this.createTimestamp(entry));
            this.setState({
              equipmentDataHourly: equipmentObjCopyHourly,
            });
            return data.value;
          })
          .catch((e) => {
            console.log(e);
            return e;
          });
      });
      equipmentObjDaily.forEach((element, index) => {
        fetch(this.proxyUrl + element.equipment.url)
          .then((blob) => blob.json())
          .then((data) => {
            equipmentObjCopyDaily[index].equipment.data = data.value;
            // Convert W and Wh to kW and kWh
            equipmentObjCopyDaily[index].equipment.data = equipmentObjCopyDaily[
              index
            ].equipment.data.map((entry) => this.wToKW(entry));
            // Round numbers
            equipmentObjCopyDaily[index].equipment.data = equipmentObjCopyDaily[
              index
            ].equipment.data.map((entry) => this.roundObject(entry));
            // Convert year, month, day, and hour to proper timestamp
            equipmentObjCopyDaily[index].equipment.data = equipmentObjCopyDaily[
              index
            ].equipment.data.map((entry) => this.createTimestamp(entry));
            this.setState({
              equipmentDataDaily: equipmentObjCopyDaily,
            });
            return data.value;
          })
          .catch((e) => {
            console.log(e);
            return e;
          });
      });
    }
  };

  wToKW = (obj) => {
    // Convert W and Wh to kW and kWh
    obj["avg_power"] = obj["avg_power"] / 1000;
    obj["cumulated_pos_consumption"] = obj["cumulated_pos_consumption"] / 1000;
    return obj;
  };

  roundObject = (obj) => {
    // Rounds all numbers
    let newObj = obj;
    for (let key in obj) {
      if (typeof newObj[key] === "number" && newObj[key] % 1 !== 0) {
        newObj[key] = newObj[key].toFixed(2);
      }
    }
    return newObj;
  };

  createTimestamp = (row) => {
    const date = new Date(row.year, 0); // initialize a date in `year-01-01`
    // Convert the day of the year (day column) to an actual date
    const timestamp = new Date(date.setDate(row.day)); // add the number of days
    if (row.hour != null) {
      timestamp.setHours(row.hour);
      row["timestamp"] =
        timestamp.getFullYear() +
        "-" +
        (timestamp.getMonth() + 1) +
        "-" +
        timestamp.getDate() +
        " h:" +
        timestamp.getHours();
    } else {
      row["timestamp"] =
        timestamp.getFullYear() +
        "-" +
        (timestamp.getMonth() + 1) +
        "-" +
        timestamp.getDate();
    }

    return row;
  };

  handleMeasureChange = (e) => {
    e.preventDefault();
    let measures = [];
    e.detail.items.forEach((item) => {
      measures.push(item.text);
    });
    if (!measures.includes("energy_pos")) {
      this.setState({ showTarget: false });
    } else {
      this.setState({ showTarget: true });
    }
    let result = [];
    measures.map((measure) => {
      // Create a displayable object from each measure
      let accessor = "";
      let color = "";

      switch (measure) {
        case "current":
          accessor = "avg_current";
          color = "red";
          break;
        case "voltage":
          accessor = "avg_voltage";
          color = "blue";
          break;
        case "power":
          accessor = "avg_power";
          color = "green";
          break;
        case "frequency":
          accessor = "avg_frequency";
          color = "purple";
          break;
        case "energy_pos":
          accessor = "cumulated_pos_consumption";
          color = "orange";
          break;
        default:
          alert("Unknown measure passed");
      }
      result.push({ accessor: accessor, color: color });
    });

    this.setState({ chosenMeasures: result });
  };

  chooseDataSet = () => {
    const hourlyScope = this.state.chosenScope === "hourly";
    const equipment = this.state.chosenEquipment;
    if (
      this.state.equipmentDataHourly &&
      this.state.equipmentDataDaily &&
      this.state.allDataHourly &&
      this.state.allDataDaily
    ) {
      switch (equipment) {
        case 1:
          {
            hourlyScope
              ? this.setState({
                  currentDataSet: this.state.equipmentDataHourly[0].equipment
                    .data,
                  currentTarget: this.props.settings.line1Target / 24,
                })
              : this.setState({
                  currentDataSet: this.state.equipmentDataDaily[0].equipment
                    .data,
                  currentTarget: this.props.settings.line1Target,
                });
          }

          break;
        case 2:
          {
            hourlyScope
              ? this.setState({
                  currentDataSet: this.state.equipmentDataHourly[1].equipment
                    .data,
                  currentTarget: this.props.settings.line2Target / 24,
                })
              : this.setState({
                  currentDataSet: this.state.equipmentDataDaily[1].equipment
                    .data,
                  currentTarget: this.props.settings.line2Target,
                });
          }
          break;
        case 3:
          {
            hourlyScope
              ? this.setState({
                  currentDataSet: this.state.equipmentDataHourly[2].equipment
                    .data,
                  currentTarget: this.props.settings.line3Target / 24,
                })
              : this.setState({
                  currentDataSet: this.state.equipmentDataDaily[2].equipment
                    .data,
                  currentTarget: this.props.settings.line3Target,
                });
          }

          break;
        default: {
          hourlyScope
            ? this.setState({
                currentDataSet: this.state.allDataHourly,
                currentTarget: this.props.settings.allLinesTarget / 24,
              })
            : this.setState({
                currentDataSet: this.state.allDataDaily,
                currentTarget: this.props.settings.allLinesTarget,
              });
        }
      }
      this.setState({ allDataShown: true });
    } else {
      alert("Not all data fetched yet. Wait for a few seconds and try again.");
    }
  };

  handleEquipmentChange = (e) => {
    const equipment = e.target._state._text;

    let result = 0;
    switch (equipment) {
      case "line 1":
        result = 1;
        break;
      case "line 2":
        result = 2;
        break;
      case "line 3":
        result = 3;
        break;
      default:
        result = 0;
    }
    this.setState({ chosenEquipment: result }, () => this.chooseDataSet());
  };

  handleScopeChange = (e) => {
    this.setState({ chosenScope: e.target._state._text }, () =>
      this.chooseDataSet()
    );
  };

  componentDidMount() {
    // Fetch initial data to have something to show
    if (this.useLocalData) {
      let data = completeHourly20;
      data = data.map((entry) => this.wToKW(entry));
      // Round values
      data = data.map((entry) => this.roundObject(entry));
      // Convert year, month, day, and hour to proper timestamp
      data = data.map((entry) => this.createTimestamp(entry));

      this.setState({
        currentDataSet: data,
        currentTarget: this.props.settings.allLinesTarget / 24,
      });
    } else {
      fetch(
        this.proxyUrl +
          this.hanaUrl +
          "/data/completedata/Complete_Data_Hourly?$top=20"
      )
        .then((blob) => blob.json())
        .then((data) => {
          // Convert W and Wh to kW and kWh
          data.value = data.value.map((entry) => this.wToKW(entry));
          // Round values
          data.value = data.value.map((entry) => this.roundObject(entry));
          // Convert year, month, day, and hour to proper timestamp
          data.value = data.value.map((entry) => this.createTimestamp(entry));

          this.setState({
            currentDataSet: data.value,
            currentTarget: this.props.settings.allLinesTarget / 24,
          });
        })
        .catch((err) => console.log(err));
    }

    // Then in the background fetch a bigger number of items for different scenarios (also daily, different equipments)
    this.fetchData(0);
  }

  render() {
    return (
      <React.Fragment>
        <FlexBox
          justifyContent={FlexBoxJustifyContent.Center}
          alignItems={FlexBoxAlignItems.Center}
          className="content-container"
        >
          <this.FormInputWrapper>
            <Select onChange={this.handleScopeChange}>
              <Option>hourly</Option>
              <Option>daily</Option>
            </Select>
          </this.FormInputWrapper>
          <this.FormInputWrapper>
            <Select onChange={this.handleEquipmentChange}>
              <Option>all lines</Option>
              <Option>line 1</Option>
              <Option>line 2</Option>
              <Option>line 3</Option>
            </Select>
          </this.FormInputWrapper>
          <this.FormInputWrapper>
            <MultiComboBox
              placeholder="Choose measures"
              onSelectionChange={this.handleMeasureChange}
            >
              <MultiComboBoxItem text="current" />
              <MultiComboBoxItem text="voltage" />
              <MultiComboBoxItem text="power" />
              <MultiComboBoxItem text="frequency" />
              <MultiComboBoxItem text="energy_pos" />
            </MultiComboBox>
          </this.FormInputWrapper>
          {this.state.allDataShown ? (
            <></>
          ) : (
            <this.FormInputWrapper>
              <Button
                onClick={() => {
                  // Data has been fetched in the background
                  this.chooseDataSet();
                }}
              >
                Load All Data
              </Button>
            </this.FormInputWrapper>
          )}
        </FlexBox>
        <LineChart
          dimensions={[{ accessor: "timestamp" }]}
          measures={this.state.chosenMeasures}
          dataset={this.state.currentDataSet}
          style={{ width: "100%", marginTop: "20px", marginBottom: "20px" }}
          chartConfig={{
            referenceLine: {
              color: "red",
              label: "Target (cumulated_pos_consumption)",
              value: this.state.showTarget ? this.state.currentTarget : null,
            },
            zoomingTool: true,
          }}
        />
        <div className="content-container">
          <Table
            columns={
              <>
                <TableColumn style={{ width: "12rem" }}>
                  <Label>timestamp</Label>
                </TableColumn>
                {this.state.chosenEquipment === 0 ? (
                  <></>
                ) : (
                  <TableColumn popinText="equipment_ID">
                    <Label>equipment_ID</Label>
                  </TableColumn>
                )}

                <TableColumn popinText="current" demandPopin>
                  <Label>current (A)</Label>
                </TableColumn>
                <TableColumn popinText="voltage" demandPopin>
                  <Label>voltage (V)</Label>
                </TableColumn>
                <TableColumn>
                  <Label>power (kW)</Label>
                </TableColumn>
                <TableColumn>
                  <Label>frequency (Hz)</Label>
                </TableColumn>
                <TableColumn>
                  <Label>energy_pos (kWh)</Label>
                </TableColumn>
              </>
            }
          >
            {this.state.currentDataSet
              ? this.state.currentDataSet.map((entry, i) => (
                  <TableRow key={i}>
                    <TableCell>
                      <Label>{entry.timestamp}</Label>
                    </TableCell>
                    {this.state.chosenEquipment === 0 ? (
                      <></>
                    ) : (
                      <TableCell>
                        <Label>{entry.equipment_ID}</Label>
                      </TableCell>
                    )}
                    <TableCell>
                      <Label>{entry.avg_current}</Label>
                    </TableCell>
                    <TableCell>
                      <Label>{entry.avg_voltage}</Label>
                    </TableCell>
                    <TableCell>
                      <Label>{entry.avg_power}</Label>
                    </TableCell>
                    <TableCell>
                      <Label>{entry.avg_frequency}</Label>
                    </TableCell>
                    <TableCell>
                      <Label>{entry.cumulated_pos_consumption}</Label>
                    </TableCell>
                  </TableRow>
                ))
              : "Loading..."}
          </Table>
        </div>
      </React.Fragment>
    );
  }
}

export default Timeline;
