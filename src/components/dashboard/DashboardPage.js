import React, { useRef, useCallback } from "react";
import { Popover } from "@ui5/webcomponents-react";
import Dashboard from "./Dashboard";
import CreateDashboardPopup from "../forms/CreateDashboardPopup";
import styled from "styled-components";
import { BsThreeDots } from "react-icons/bs";
import Timeline from "../maindashboard/Timeline";
import AddGadgetPopup from "../forms/AddGadgetPopup";
import "@ui5/webcomponents-icons/dist/icons/add";
import { Button } from "@ui5/webcomponents-react/lib/Button";

const ActionContainer = styled.div`
  box-sizing: border-box;
  display: table-cell;
  padding: 0;
  text-align: right;
`;

const DashboardHeader = styled.div`
  border-spacing: 0;
  box-sizing: border-box;
  display: table;
  table-layout: auto;
  width: 100%;
  padding: 10px 0;
`;

const DashboardTitle = styled.div`
  font-family: var(--sapFontFamily);
  font-size: var(--sapFontSize);
  font-size: 30px;
`;

const Action = styled.div`
  box-sizing: border-box;
  display: inline-block;
  border-radius: 3px;
  text-decoration: none;
  font-family: var(--sapFontFamily);
  font-size: var(--sapFontSize);
  text-align: center;
  background-color: #f4f5f7;
  margin-left: 10px;
  margin-right: 10px;
  padding: 4px;
  font-size: 21px;
`;

function DashboardPage(props) {
  const popoverRef = useRef();
  const onButtonClick = useCallback(
    (e) => {
      popoverRef.current.openBy(e.target);
    },
    [popoverRef]
  );

  return (
    <React.Fragment>
      <div className="dashboard-container">
        <DashboardHeader>
          <DashboardTitle>
            {props.dashboard ? props.dashboard.name : "Main Dashboard"}
          </DashboardTitle>
          <ActionContainer>
            {props.dashboard ? (
              <Action className="action">
                <AddGadgetPopup
                  onAddGadget={props.onAddGadget}
                  gadgets={props.gadgets}
                  gadgetsCount={props.gadgets.length}
                  gadgetTypes={["Prediction", "LineChart", "KPI", "Text Field"]}
                  measureTypes={["Energy Consumption", "Off-Peak Percentage"]}
                  scopeTypes={{
                    consumption: [
                      "AllTime_Hourly",
                      "AllTime_Daily",
                      "AllTime_Weekly",
                      "AllTime_Monthly",
                      "AllTime_Quarterly",
                      "AllTime_Yearly",
                      "Hourly_ThisWeek",
                      "Hourly_ThisMonth",
                      "Hourly_ThisYear",
                      "Daily_ThisWeek",
                      "Daily_ThisMonth",
                      "Daily_ThisYear",
                      "Weekly_ThisMonth",
                      "Weekly_ThisYear",
                      "Monthly_ThisYear",
                      "Quarterly_ThisYear",
                    ],
                    offpeak: [
                      "AllTime_Weekly",
                      "AllTime_Monthly",
                      "AllTime_Quarterly",
                      "AllTime_Yearly",
                    ],
                  }}
                  dashboardId={props.dashboard.id}
                  settings={props.settings}
                  hanaURL={props.hanaURL}
                />
              </Action>
            ) : (
              ""
            )}

            <Action className="action">
              <a
                onClick={onButtonClick}
                style={{ color: "black", textDecoration: "none" }}
                to="/add"
              >
                <BsThreeDots size="20" />
              </a>
            </Action>
          </ActionContainer>
        </DashboardHeader>
        {props.dashboard ? (
          <Dashboard
            key={props.dashboard.id}
            settings={props.settings}
            dashboardId={props.dashboard.id}
            onDeleteGadget={props.onDeleteGadget}
            onAddGadget={props.onAddGadget}
            onUpdateGadget={props.onUpdateGadget}
            gadgets={props.gadgets.filter(
              (g) => g.dashboard === props.dashboard.id
            )}
            gadgetsCount={props.gadgets.length}
            gadgetTypes={props.gadgetTypes}
            measureTypes={props.measureTypes}
            scopeTypes={props.scopeTypes}
            proxy={props.proxy}
            hanaURL={props.hanaURL}
          />
        ) : (
          <Timeline
            settings={props.settings}
            proxy={props.proxy}
            hanaURL={props.hanaURL}
          />
        )}
      </div>
      <Popover
        className="popover"
        ref={popoverRef}
        noArrow={true}
        placement-type="Bottom"
        horizontalAlign="Right"
      >
        <div className="popover-content">
          <ui5-list separators="None">
            <CreateDashboardPopup
              onCreateDashboard={props.onCreateDashboard}
              dashboards={props.dashboards}
            >
              Create Dashboard
            </CreateDashboardPopup>
            {props.dashboard ? (
              <Button
                className="delete-button"
                onClick={() => props.onDeleteDashboard(props.dashboard)}
              >
                Delete Dashboard
              </Button>
            ) : (
              ""
            )}
          </ui5-list>
        </div>
      </Popover>
    </React.Fragment>
  );
}

export default DashboardPage;
