import React, { Component } from "react";
import LineChartContainer from "../gadgets/linechart/LineChartContainer";
import KPIContainer from "../gadgets/kpi/KPIContainer";
import MarkupGadgetContainer from "../gadgets/markup/MarkupGadgetContainer";
import Placeholder from "../maindashboard/Placeholder";

import { ThemeProvider } from "@ui5/webcomponents-react";

class Dashboard extends Component {
  render() {
    return (
      <div className="dashboard">
        <ThemeProvider>
          <MarkupGadgetContainer
            onDeleteGadget={this.props.onDeleteGadget}
            onUpdateGadget={this.props.onUpdateGadget}
            gadgets={this.props.gadgets.filter((g) => g.type === "markup")}
            proxy={this.props.proxy}
          />
          <KPIContainer
            onDeleteGadget={this.props.onDeleteGadget}
            gadgets={this.props.gadgets.filter((g) => g.type === "KPI")}
            proxy={this.props.proxy}
            hanaURL={this.props.hanaURL}
          />
          <LineChartContainer
            onDeleteGadget={this.props.onDeleteGadget}
            proxy={this.props.proxy}
            hanaURL={this.props.hanaURL}
            onUpdateGadget={this.props.onUpdateGadget}
            gadgets={this.props.gadgets.filter(
              (g) => g.type === "linechart2" || g.type === "prediction"
            )}
          />
          <Placeholder
            onAddGadget={this.props.onAddGadget}
            gadgets={this.props.gadgets}
            gadgetsCount={this.props.gadgetsCount}
            gadgetTypes={this.props.gadgetTypes}
            measureTypes={this.props.measureTypes}
            scopeTypes={this.props.scopeTypes}
            dashboardId={this.props.dashboardId}
            settings={this.props.settings}
            style={{ marginTop: "50px" }}
          />
        </ThemeProvider>
      </div>
    );
  }
}

export default Dashboard;
